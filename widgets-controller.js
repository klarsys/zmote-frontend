angular.module('zMoteApp')
    .controller("WidgetsController", function($scope, $location, $timeout, $state, $stateParams, localStorageService, rdb, $mdDialog, $http, $window) {
        $scope.hint = "fetching zmotes...";
        $scope.palettes = rdb.palettes;
        $scope.nowidgets = false;

        $scope.gotoWidget = function(event, widget) {
            if (widget.needsUpdate)
                $state.go(rdb.is.state + 'update', {
                    widgetId: widget._id
                })
            else
                $state.go(rdb.is.state + 'widget', {
                    widgetId: widget._id
                });
        };
        $scope.refresh = function () {
            $scope.nowidgets = false;
            rdb.updateWidgets().then(
                function(){
                    autorouter();
                });
        };
        var autorouter = function() {
            if (!rdb.status.widgetsUpdated) {
                $timeout(autorouter, 500);
                return;
            }
            if (rdb.w.widgets.length == 1 && !rdb.w.widgets[0].needsUpdate) { 
                // only one widget. go there
                var widget = rdb.w.widgets[0];
                if ($window.isServedFromWidget) {
                    $state.go(rdb.is.state + 'configure', {
                        widgetId: widget._id
                    });
                    return;
                } else {
                    if (widget.gadgets.length == 0 && !widget.beenToAddGadget) {
                        // if there are no gadgets, goto add gadget page
                        $state.go(rdb.is.state + 'addgadget', {
                            widgetId: widget._id
                        });
                    }
                    if (widget.gadgets.length == 1 && !widget.beenToViewWidget) {
                        $state.go(rdb.is.state + 'widget', {
                            widgetId: widget._id
                        });
                    }
                    return;
                }
            }
            if (rdb.w.widgets.length == 0) $scope.nowidgets = true;
            return;
        };
        autorouter();
    });
