
var reset = function() {
    networks = [];
    base = '/';
//    base = 'http://192.168.1.3/';
    stamac = '';
    info = '';
    name = '';
    maxtries = 1;
}

function addButtons(n, i) {
    var elem = document.createElement("div"); // Create with DOM
    elem.setAttribute('class', 'form-mini-container form-mini allbuttons');
    elem.innerHTML = '<button onclick="selectNetwork(' + i + ')">' + n.ssid + '</button>';
    $(".main-content").append(elem); // Insert new elements after <img>
};

function showForm(i) {
    var n = networks[i];
    // hide all the buttons
    $("#buttons").hide();
    // show one form
    var elem = document.createElement("div"); // Create with DOM
    elem.setAttribute('class', 'form-mini-container');
    elem.setAttribute('id', 'credform');
    elem.innerHTML = '<h1>' + n.ssid + '</h1>' + '<div class="form-mini">' + '<div class="form-row">' + '<input id="password" type="password" name="name" placeholder="enter password">' + '</div>' + '<div class="form-row form-last-row">' + '<button onclick="saveNetwork(' + i + ')">Connect</button>' + '<button onclick="cancelNetwork(' + i + ')">Cancel</button>' + '</div>' + '</div>';
    $(".main-content").append(elem); // Insert new elements after <img>
}

var selectNetwork = function(i) {
    console.log('selected network', networks[i].ssid);
    $(".allbuttons").hide();
    $(".initialhint").hide();
    showForm(i);
};

var cancelNetwork = function(i) {
    $("div").remove('#credform');
    $(".allbuttons").show();
}

var saveNetwork = function(i) {
    console.log("Saving...");
    $("#buttons").hide();
    var p = $('#password').val();
    $("div").remove('#credform'); // remove form
    $(".screentip").text("Saving..."); //
    $(".three-quarters-loader").hide(); //
    $(".spinner").show();

    $.post(base + stamac + '/api/wifi/connect', JSON.stringify({
            ssid: networks[i].ssid,
            password: p
        }), function(response) {
            console.log('Configuring Wifi network...');
            $(".screentip").text("Configuring Wifi network..."); //
            console.log(response);
            setTimeout(function() {
                $(".screentip").text('Connecting to new network...');
                checkConnection();
            }, 2000);
        }, "json")
        .fail(function() {
            $(".screentip").text("Failed to connect. Refresh.");
        })
};

var checkConnection = function() {
    $.getJSON(base + 'api/wifi/mac', function() {
        // Still connected to the hotspot.  Try after a while
        setTimeout(checkConnection, 500);
    }).fail(function() {
        // AP disconnected
        //$window.location.reload();
        window.location.href = "http://www.zmote.io/app";
    });
};


var saveName = function() {
    console.log('Saving...');
    $.post(base + stamac + '/api/wifi/config', JSON.stringify({
            ssid: name
        }), function(response) {
            timer = $timeout($scope.searchAgain, 2000);
        }, "json")
        .fail(function(error) {
            console.log('error', error);
        });
};


var getWiFiNetworks = function(retries) {
    if (retries >= 0) {
        if (retries == 0) {
            $(".screentip").text("Failed to connect. Refresh.");
            $(".three-quarters-loader").hide();
        }
    } else {
        retries = maxtries;
    }
    console.log('Scanning Wi-Fi networks...')
    $.getJSON(base + stamac + '/api/wifi/scan', function(response) {
        networks = response;
        networks.sort(function(a, b) {
            return b.rssi - a.rssi;
        });
        console.log(networks);
        $(".spinner").hide();
        networks.forEach(function(n, i) {
            addButtons(n, i);
            console.log(n.rssi);
        });
    }).fail(function(error) {
        console.log(error);
        setTimeout(function() {
            getWiFiNetworks(--retries)
        }, 1000);
    });
};

var getWiFiStatus = function(retries) {
    if (retries >= 0) {
        if (retries == 0) {
            $(".screentip").text("Failed to connect. Refresh.");
            $(".three-quarters-loader").hide();
        }
    } else {
        retries = maxtries;
    }
    console.log('Reading configuration...')
    $.getJSON(base + stamac + '/api/wifi/status', function(response) {
        console.log(response);
        info = response;
        name = info.ap_ssid;
        getWiFiNetworks();
    }).fail(function(error) {
        console.log(error);
        setTimeout(function() {
            getWiFiStatus(--retries)
        }, 1000);
    });
};

var connectToWidget = function(retries) {
    if (retries >= 0) {
        if (retries > 0) {
            $(".screentip").text("Trying to Connect again...");
        } else {
            $(".screentip").text("Failed to connect. Refresh.");
            $(".three-quarters-loader").hide();
            return;
        }
    } else {
        retries = maxtries;
        $(".screentip").text("Connecting...");
    }

    console.log('Connecting...');
    var jqxhr = $.getJSON(base + 'api/wifi/mac', function(response) {
            console.log(response);
            if (response.sta_mac) {
                stamac = response.sta_mac;
                getWiFiStatus();
                success = true;
            }
        })
        .fail(function(response) {
            console.log("error", response);
            setTimeout(function() {
                connectToWidget(--retries)
            }, 1000);
        });
};
