angular.module('zMoteApp')
    .controller("AppController", function($scope, $state, $stateParams, rdb, $mdToast, $timeout, $window, $location) {
        $scope.w = rdb.w;
        rdb.is.state = "app.";
        var url = $location.$$url;

        if (url.substr(0, "/demo".length) == "/demo") {
            rdb.is.state = "demo.";
            rdb.is.demo = true;
        } else {
            rdb.is.demo = false;
        }

        if ($window.isDemo !== rdb.is.demo) {
            $window.isDemo = rdb.is.demo;
            if (rdb.status.authenticated) {
                rdb.removeWidgets();
                rdb.updateWidgets();
            }
        }

        $scope.$watch(function() {
            return $location.$$url;
        }, function() {
            url = $location.$$url;
            if ($stateParams.widgetId) {
                var widget = _.find(rdb.w.widgets, function(widget) {
                    return widget._id == $stateParams.widgetId;
                });

                if (widget && widget.needsUpdate && url.substr(url.length-'update'.length) != 'update') {
                    // goto widgets. widgets controller will redirect to update if needed.
                    $state.go(rdb.is.state + 'widgets', {
                        widgetId: widget._id
                    })
                    return;
                }
            }
            if ($window.isServedFromWidget || url == "/demo" || url == "/app") {
                $state.go(rdb.is.state + 'widgets');
                return;
            }
        });
    });
