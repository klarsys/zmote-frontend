angular
    .module('zMoteApp')
    .controller('ZMoteConfigureController', ['$scope', '$state', '$stateParams', '$http', '$timeout', '$mdToast', 'rdb', function($scope, $state, $stateParams, $http, $timeout, $mdToast, rdb) {

        var timer = null;
        $scope.$on("$destroy", function(event) {
            hideBusy();
            $timeout.cancel(timer);
            timer = null;
        });

        $scope.reset = function() {
            $scope.stamac = null;
            $scope.info = null;
            $scope.name = null;
            $scope.networks = [];
            $scope.ssid = null;
            $scope.password = '';
            $scope.error = '';
            $scope.local = false;
        }

        $scope.goBack = function() {
            $state.go('home');
        };

        $scope.searchAgain = function() {
            $scope.reset();
            connectToWidget();
        };

        $scope.saveName = function() {
            showBusy('Saving...');
            if ($scope.local) {
                post('http://' + $scope.widget.localIP + '/' + $scope.stamac + '/api/wifi/config', {
                        ssid: $scope.name
                    })
                    .then(function(response) {
                        // console.log(response.data);
                        $scope.configNameForm.$setPristine()
                        timer = $timeout($scope.searchAgain, 2000);
                    })
                    .catch(function(error) {
                        // FIXME
                        console.log('Configure:', error);
                        hideBusy();
                    });
            } else {
                // FIXME
            }
        };

        $scope.selectNetwork = function(ssid) {
            $scope.ssid = ssid;
        };

        $scope.saveWifi = function() {
            showBusy('Saving...');
            if ($scope.local) {
                post('http://' + $scope.widget.localIP + '/' + $scope.stamac + '/api/wifi/connect', {
                        ssid: $scope.ssid,
                        password: $scope.password
                    })
                    .then(function(response) {
                        // console.log(response.data);
                        $scope.reset();
                        $scope.configWifiForm.$setPristine()
                        showBusy('Configuring Wifi network...');
                        // FIXME: Refresh widgets
                        timer = $timeout(function(){
                            showBusy('Connecting to new network...');
                            timer = $timeout(getWidget, 8000);
                        }, 5000);
                    })
                    .catch(function(error) {
                        // FIXME
                        console.log('Configure:', error);
                        hideBusy();
                    });
            } else {
                // FIXME
            }
        };

        var get = function(url, config) {
            // console.log('GET', url);
            return $http.get(url, config);
        };

        var put = function(url, data, config) {
            // console.log('PUT', url);
            // console.log(data);
            return $http.put(url, data, config);
        };

        var post = function(url, data, config) {
            // console.log('POST', url);
            // console.log(data);
            return $http.post(url, data, config);
        };

        var toastVisible = false;
        var showBusy = function(message) {
            if (toastVisible)
                $mdToast.updateContent(message);
            else {
                toastVisible = true;
                $mdToast.show($mdToast.simple().content(message).hideDelay(0));
            }
        };
        var hideBusy = function() {
            $mdToast.hide();
            toastVisible = false;
        };

        var getWiFiNetworks = function() {
            showBusy('Scanning Wi-Fi networks...')
            if ($scope.local) {
                get('http://' + $scope.widget.localIP + '/' + $scope.stamac + '/api/wifi/scan')
                    .then(function(response) {
                        $scope.networks = response.data;
                        for (var i = $scope.networks.length - 1; i >= 0; i--) {
                            if ($scope.networks[i].connected === true)
                                $scope.ssid = $scope.networks[i].ssid;
                        };
                        hideBusy();
                    })
                    .catch(function(error) {
                        // FIXME: MQTT
                        console.log('Configure:', error);
                        hideBusy();
                    })
            } else {
                // FIXME
            }
        };

        var getWiFiStatus = function() {
            showBusy('Reading configuration...')
            if ($scope.local) {
                get('http://' + $scope.widget.localIP + '/' + $scope.stamac + '/api/wifi/status')
                    .then(function(response) {
                        $scope.info = response.data;
                        $scope.name = $scope.info.ap_ssid;
                        getWiFiNetworks();
                    })
                    .catch(function(error) {
                        // FIXME: MQTT
                        console.log('Configure:', error);
                        hideBusy();
                    })
            } else {
                // FIXME
            }
        };

        var connectToWidget = function() {
            showBusy('Connecting...');
            get('http://' + $scope.widget.localIP + '/api/wifi/mac', {
                    timeout: 3000
                })
                .then(function(response) {
                    $scope.error = '';
                    $scope.stamac = response.data.sta_mac;
                    chipID = '00' + $scope.stamac.replace(/-/g, '').substring(6, 12)
                    if (chipID === $scope.widget.chipID)
                        $scope.local = true;
                    getWiFiStatus();
                }, function(error) {
                    // FIXME: MQTT
                    $scope.error = 'Could not establish connection';
                    console.log('Configure:', error);
                    hideBusy();
                    if (timer) timer = $timeout(connectToWidget, 3000); // Hack
                });
        };

        var getWidget = function() {
            $scope.widget = null;
            $scope.reset();

            var windex = _.findIndex(rdb.w.widgets, function(widget) {
                return widget._id == $stateParams.widgetId;
            })
            if (windex >= 0) {
                $scope.widget = rdb.w.widgets[windex];
                $scope.searchAgain();
            } else
                timer = $timeout(getWidget, 500);
        };

        showBusy('Loading...');
        timer = $timeout(getWidget, 0); // Hack
    }]);
