var remotes = require('./remotes.json');
var fs = require('fs');

var db = {};

db.headers = [];
db.remotes = [];

remotes.forEach(function(remote,i){
	db.headers.push({brand:remote.brand,model:remote.model,name:remote.name,confidence:remote.confidence,id:i});
	remote.id = i;
	db.remotes.push(remote);
});

fs.writeFile("./zmotedb.json", JSON.stringify(db), function(err) {
    if(err) {
        return console.log(err);
    }
    console.log("The file was saved!");
}); 

