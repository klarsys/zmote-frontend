angular.module('zMoteApp').factory("rdb",
    function($http, localStorageService, $interval, $timeout, $q, $mdToast, ApiBase, notify) {
        var register = {};
        localStorageService.get('register');
        //var ws = localStorageService.get('widgets');
        var w = {
            widgets: []
        };
        var loop = null;
        var q;
        var is = {
            demo: false,
            state: 'app'
        };
        var status = {
            authenticated: false,
            widgetsUpdated: false,
        };
        var connectedFlagTimer = false;

        var clientRegister = function() {
            register = localStorageService.get('register');
            if (register == null || ((Date.now() - Date.parse(register.created)) > 90 * 24 * 60 * 60 * 1000)) {
                return $http.get(ApiBase + "/client/register")
                    .then(
                        function(response) {
                            if (response.data.secret) {
                                register = response.data;
                                localStorageService.set('register', register);
                            }
                            return true;
                        },
                        function(resp) {
                            console.log('err in /client/register');
                            throw (new Error(resp));
                            return false;
                        });
            } else return $q.when(true);
        };

        var authenticate = function() {
            return clientRegister().then(function() {
                    // authenticate
                    return $http.post(ApiBase + "/client/auth", register);
                })
                .then(function(resp) {
                    console.log("auth successful", resp);
                    console.log("extIP", resp.data.extIP); 
                    status.authenticated = true;
                    status.extIP = resp.data.extIP;
                    return updateWidgets();
                }, function(resp) {
                    console.log("auth err. retrying", resp);
                    localStorageService.remove('register');
                    return $timeout(authenticate, 500);
                });
        };

        var checkConnectedFlag = function(widget) {
            if (connectedFlagTimer) $timeout.cancel(connectedFlagTimer);
            if (!widget) {
                notify.closeAll();
                return;
            }
            if (is.demo) return;

            var req = ApiBase + "/widgets";
            var notconnectedmsg = "Unable to communicate with zmote.";
            //if (!retries) retries = 4;
            var showNotify = function(msg) {
                notify.closeAll();
                notify({
                    message: msg,
                    classes: ['notificationbar']
                });
            }

            if (widget.local) {//if (widget.extIP === status.extIP) { // local connection
                // use localIP/wifi/mac to check connectivity
                $http.get("http://" + widget.localIP + "/api/wifi/mac", {
                        timeout: 5000
                    })
                    .then(function() {
                        return true;
                    }, function() {
                        return false;
                    })
                    .then(function(response) {
                        if (!response)
                            showNotify(notconnectedmsg);
                        connectedFlagTimer = $timeout(function() {
                            checkConnectedFlag(widget)
                        }, 10000);
                    });
            } else { // external connection. check the flag
                $http.get(req)
                    .then(function(response) {
                            var widgets = response.data;
                            w = _.find(widgets, function(w) {
                                return w._id === widget._id;
                            });
                            if (!w || !w.connected)
                                showNotify(notconnectedmsg);
                        },
                        function(response) {
                            showNotify(notconnectedmsg);
                            connectedFlagTimer = $timeout(function() {
                                checkConnectedFlag(widget)
                            }, 10000);
                        });
            }
        };

        var removeWidgets = function() {
            w.widgets = [];
        }

        var checkOTA = function(widget) {
            widget.needsUpdate = false;
            if (is.demo || !widget.connected)
                return;
            $http.get(ApiBase + '/ota/firmware')
                .then(function(resp) {
                    is.ota = resp.data;
                    return $http.get(ApiBase + '/ota/fs');
                })
                .then(function(resp) {
                    is.ota.fs_version = resp.data.fs_version;
                    if (widget.fs_version == "update")
                        widget.fs_version = 0;
                    console.log("widget update check: ", is.ota, widget.version, widget.fs_version);
                    widget.fwUpdate = (widget.version < is.ota.version);
                    widget.fsUpdate = (widget.fs_version < is.ota.fs_version);
                    //widget.needsUpdate = (widget.fwUpdate || widget.fsUpdate);
                    widget.needsUpdate = widget.fwUpdate; // FIXME do not update FS for now
                    if (widget.needsUpdate)
                        console.log("widget needs update:" + widget._id);
                });
        };
        var pollForOTAFinish = function(widget, key, start) {
            console.log("Polling " + key + " start=" + start + " cur=" + Date.now());
            return $timeout(function() {
                    return true
                }, 1000)
                .then(function() {
                    return updateWidgets();
                })
                .then(function() {
                    widget = _.find(w.widgets, function(w) {
                        return widget._id === w._id;
                    });
                    if (!widget)
                        throw (new Error("Widget dissappeared"));
                    console.log("Poll OTA [" + key + "]: ", widget[key], is.ota[key]);
                    if (widget[key] == is.ota[key])
                        return true;
                    if (start < Date.now() - 3 * 60 * 1000) // 3 minute timeout
                        throw (new Error("Update " + key + " timeout"));
                    return pollForOTAFinish(widget, key, start);
                });
        };
        var updateFS = function(widget) {
            return $q.when(true); // DIsable FS update for now
            console.log("Updating FS", widget, ApiBase + '/ota/fs');
            return $http.post(ApiBase + '/widgets/' + widget._id + '/ota', {
                    fs_version: is.ota.fs_version
                })
                //return $http.get(ApiBase + '/ota/fs')
                .then(function() {
                    console.log("here");
                    return pollForOTAFinish(widget, 'fs_version', Date.now());
                });
            //return $timeout(function () {return true}, 3000);
        };
        var updateFW = function(widget) {
            console.log("Updating FW", widget);
            return $http.post(ApiBase + '/widgets/' + widget._id + '/ota', {
                    version: is.ota.version
                })
                //$http.get(ApiBase + '/ota/firmware')
                .then(function() {
                    return pollForOTAFinish(widget, 'version', Date.now());
                });
            return $timeout(function() {
                return true
            }, 3000);
        };

        var isWidgetLocal = function(widget, retries) {
            if (!retries) retries = 4;
            if (retries <= 0) {
                return;
            }
            // use localIP/wifi/mac to check connectivity
            $http.get("http://" + widget.localIP + "/api/wifi/mac", {
                    timeout: 5000
                })
                .then(function(resp) {
                    chipID = '00' + resp.data.sta_mac.toLowerCase().substr(9).replace(/-/g, '');
                    if (chipID == widget.chipID) widget.local = true;
                    else widget.local = false;
                    console.log('zmote local',widget.local);
                    return true;
                }, function() {
                    // retry
                    $timeout(function() {
                        isWidgetLocal(widget,retries--)
                    }, 500);
                });
        };

        var updateWidgets = function() {
            status.widgetsUpdated = false;
            var req = (is.demo) ? ApiBase + "/demowidgets" : ApiBase + "/widgets";
            return $http.get(req)
                .then(function(response) {
                        w.widgets = response.data;
                        w.widgets.forEach(function(widget) {
                            checkOTA(widget);
                            widget.local = false;
                            console.log("widget",widget);
                            console.log("widget chipID, extIP, localIP are", widget.chipID, widget.extIP, widget.localIP);
                            if (widget.connected) isWidgetLocal(widget); // update widget.local here
                            updateGadgets(widget);
                        });
                        w.nConnected = w.widgets.filter(function(w) {
                            return w.connected
                        }).length;
                        w.nNotConnected = w.widgets.length - w.nConnected;
                        status.widgetsUpdated = true;
                    },
                    function(response) {
                        console.log("err in /widgets");
                        //FIXME. retry
                    });
        };

        var updateGadget = function(widget, gadgetid) {
            return $http.get(ApiBase + '/widgets/' + widget._id + '/gadgets/' + gadgetid)
                .then(
                    function(response) {
                        var gindex = _.findIndex(widget.gadgets, function(gadget) {
                            return gadget._id === gadgetid;
                        })
                        if (gindex >= 0) { // got reponse, but check if remote or user remote exist
                            if (!(widget.gadgets[gindex].userRemote || widget.gadgets[gindex].remote))
                                return false;
                            widget.gadgets[gindex] = response.data;
                        } else {
                            //console.log("not supposed to come here");
                            widget.gadgets.push(response.data);
                        }
                        return true;
                    },
                    function() {
                        console.log("err in /gadget");
                        return false;
                    });
        };

        var updateGadgets = function(widget) {
            widget.gadgetsUpdated = false;
            return $http.get(ApiBase + '/widgets/' + widget._id + '/gadgets/')
                .then(function(response) {
                        widget.gadgets = response.data;
                        for (i = widget.gadgets.length - 1; i >= 0; i--) {
                            if (!(widget.gadgets[i].remote || widget.gadgets[i].userRemote)) {
                                // remove stale gadgets with null remotes
                                widget.gadgets.splice(i, 1);
                            }
                        }
                        widget.gadgetsUpdated = true;
                    },
                    function(response) {
                        console.log("err in /gadgets");
                    });
        };

        var deleteGadget = function(widget, gadget) {
            return $http.delete(ApiBase + '/widgets/' + widget._id + '/gadgets/' + gadget._id)
                .then(
                    function(response) {
                        console.log("gadget deleted");
                        $mdToast.show($mdToast.simple('remote deleted'));
                        var gindex = _.findIndex(widget.gadgets, function(g) {
                            return g._id === gadget._id;
                        })
                        if (gindex >= 0) {
                            widget.gadgets.splice(gindex, 1);
                        } else {
                            console.log("not supposed to come here");
                        }
                        return true;
                    },
                    function(response) {
                        console.log("err in gadget deletion");
                        $mdToast.show($mdToast.simple('failed to delete the remote'));
                        return false;
                    });
        };

        var addGadget = function(widgetid, gadgetObj) {
            return $http.post(ApiBase + '/widgets/' + widgetid + '/gadgets', gadgetObj)
                .then(
                    function(resp) {
                        var windex = _.findIndex(w.widgets, function(widget) {
                            return widget._id === widgetid;
                        })
                        if (windex >= 0) {
                            return updateGadget(w.widgets[windex], resp.data._id);
                        }
                        return true;
                    },
                    function(resp) {
                        console.log("err in add gadget");
                        $mdToast.show($mdToast.simple("failed to add remote"));
                    });
        };

        var palettes = ["red", "pink", "purple", "deep-purple",
            "indigo", "blue", "light-blue", "cyan",
            "teal", "green", "light-green", "lime",
            "yellow", "amber", "deep-orange",
            "brown", "grey", "blue-grey"
        ]; // orange
        //var palettes = ["myblue","mylight-blue","mycyan"];
        /*var hues = [
              'hue-50', // use shade 50 for the <code>md-hue-50</code> class
              'hue-100', // use shade 100 for the <code>md-hue-100</code> class
              'hue-200', // use shade 200 for the <code>md-hue-200</code> class
              'hue-300','hue-400','hue-500','hue-600',
              'hue-700','hue-800','hue-900','hue-A100',
              'hue-A200','hue-A400','hue-A700'
        ];*/
        var hues = ['md-hue-1', 'md-hue-2', 'md-hue-3'];

        var makeButtonGroup = function(remote) {
            var buttongroup = [];
            var pages = 1;
            var rows = 1;
            remote.layout.forEach(function(blem) {
                if (blem === 'pagebreak') {
                    pages++;
                    rows = 1;
                } else if (blem === 'rowbreak') {
                    rows++;
                } else {
                    var b = _.find(remote.keys, function(key) {
                        return key.key === blem;
                    });
                    if (b) {
                        if (pages > buttongroup.length) {
                            buttongroup.push([]);
                            pages = buttongroup.length; // to take care of multiple/dummy pagebreaks
                        }
                        if (rows > buttongroup[pages - 1].length) {
                            buttongroup[pages - 1].push([]);
                            rows = buttongroup[pages - 1].length; // to take care of multiple/dummy rowbreaks
                        }
                        buttongroup[pages - 1][rows - 1].push(b);
                    }
                }
            });
            return buttongroup;
        };

        var saveRemote = function(widget, gadget, remote) {
            console.log(remote);

            return $http.post(ApiBase + '/widgets/' + widget._id + '/gadgets/' + gadget._id + '/userremote', remote)
                .then(function(response) {
                    console.log(response.data.userRemote);
                    var gindex = _.findIndex(widget.gadgets, function(g) {
                        return g._id === gadget._id;
                    });
                    if (gindex >= 0) {
                        widget.gadgets[gindex] = response.data;
                    } else {
                        console.log("saveRemote: gadget not found. not supposed to happen");
                    }
                    return true;
                }, function(error) {
                    console.log('FIXME: error saving remote');
                    // FIXME
                });
        };

        var getRemotes = function(query, type) {
            return $http.get(ApiBase + '/remotes', {
                    params: {
                        query: query,
                        type: type
                    }
                })
                .then(function(res) {
                    return res.data;
                });
        };

        function encode(trigger) {
            var seq = [];
            var period = 1000 / 38;
            for (var i = 0; i < trigger.length; i++) {
                seq.push(Math.floor(trigger[i] / period + 0.5));
                if (trigger[i] > 12000)
                    break;
            }
            return {
                frequency: 38000,
                n: seq.length,
                repeat: [0, 0, seq.length],
                seq: seq
            };
        }

        var getKeyCodes = function(data) {
            var toggleBitPresent = false;
            var codes = {
                confidence: 0,
                code: encode(data.trigger)
            };

            return $http.post(ApiBase + '/irp/analyse', data)
                .then(function(resp) {
                    if (resp.data.error) {
                        return codes;
                    } else {
                        return resp.data;
                    }
                })
                .catch(function(error) {
                    console.log('*** Error in IR analyse:', error);
                    return false;
                });
        };
        authenticate();

        return {
            hues: hues,
            palettes: palettes,
            w: w,
            register: register,
            removeWidgets: removeWidgets,
            updateWidgets: updateWidgets,
            updateGadgets: updateGadgets,
            updateFW: updateFW,
            updateFS: updateFS,
            makeButtonGroup: makeButtonGroup,
            saveRemote: saveRemote,
            updateGadget: updateGadget,
            deleteGadget: deleteGadget,
            addGadget: addGadget,
            getRemotes: getRemotes,
            getKeyCodes: getKeyCodes,
            is: is,
            status: status,
            checkConnectedFlag: checkConnectedFlag
        };
    });
