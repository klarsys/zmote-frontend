angular.module('zMoteApp')
    .controller("EditButtonController", function($scope, $element, rdb, wid, $timeout, $state, $stateParams, $mdToast) {
        $scope.icons = [{
            name: "no icon"
        }];
        iconnames.forEach(function(name) {
            $scope.icons.push({
                icon: name
            });
        });
        $scope.buttonelem = {};
        $scope.dirty = false;
        $scope.hint = "";

        $scope.$on("$destroy", function(event) {
            // Cancel learning (also hides prompt)
            $scope.stopLearning();
            // FIXME: Prompt for saving if dirty
        });

        var autorouter = function() {
            if (!rdb.status.widgetsUpdated) {
                $timeout(autorouter, 500);
                return;
            }
            var windex = _.findIndex(rdb.w.widgets, function(widget) {
                return widget._id === $stateParams.widgetId;
            });
            if (windex < 0) { // widget not found. goto widgets
                $state.go(rdb.is.state + 'widgets');
                return;
            }
            $scope.widget = rdb.w.widgets[windex];
            gadgetAutoroute($scope.widget);
        };

        var gadgetAutoroute = function(widget) {
            if (!widget.gadgetsUpdated) {
                $timeout(function() {
                    gadgetAutoroute(widget);
                }, 500);
                return;
            }
            $scope.gindex = _.findIndex(widget.gadgets, function(gadget) {
                return gadget._id === $stateParams.gadgetId;
            });
            if ($scope.gindex < 0) { // gadget not found. go back
                $state.go(rdb.is.state + 'widget', {
                    widgetId: widget._id
                });
                return;
            }
            $scope.gadget = $scope.widget.gadgets[$scope.gindex];
            if (!$scope.gadget.editremote) {
                // came here through page refresh, go back to editgadget page
                $state.go(rdb.is.state + 'editgadget', {
                    widgetId: widget._id,
                    gadgetId: $stateParams.gadgetId
                });
                return;
            }
            var keys = $scope.gadget.editremote.keys;
            $scope.buttonelem = angular.copy(keys[$stateParams.buttonIndex]);
            $scope.buttonelem.name = $scope.buttonelem.name;
        };

        $scope.goBack = function() {
            $state.go(rdb.is.state + 'editgadget', {
                widgetId: $stateParams.widgetId,
                gadgetId: $stateParams.gadgetId
            });
        };

        $scope.learn = function() {
            if ($scope.buttonelem.learning)
                $scope.stopLearning();
            else
                $scope.startLearning();
        };

        var toastVisible = false;
        var showPrompt = function(message) {
            if (toastVisible)
                $mdToast.updateContent(message);
            else {
                toastVisible = true;
                $mdToast.show($mdToast.simple()
                    .content(message)
                    .hideDelay(0)
                    .position('top right'));
            }
        };
        var hidePrompt = function() {
            $mdToast.hide();
            toastVisible = false;
        };

        function delay(t) {
            var d = $q.defer();
            $timeout(function() {
                d.resolve(true);
            }, t);
            return d.promise;
        }

        $scope.startLearning = function() {
            $scope.ktlCount = 0;

            var readIrCode = function() {
                $scope.buttonelem.learning = true;
                showPrompt('Press key on your real remote');
                wid.getKeyPress($scope.widget)
                    .then(function(data) {
                        if (data) {
                            $scope.buttonelem.learning = false;
                            $scope.buttonelem.analyzing = true;
                            showPrompt('Analyzing...');
                            return rdb.getKeyCodes(data);
                        }
                        // Else, cancelled
                    })
                    .then(function(codes) {
                        $scope.buttonelem.analyzing = false;
                        if (codes) {
                            // Check confidence and re-learn if required
                            if (codes.confidence == 0 && $scope.ktlCount < 2) {
                                showPrompt('Could not learn the key correctly. Let\'s try again.');
                                // Re-learn
                                $scope.ktlCount++;
                                $timeout(readIrCode, 2000);
                            } else {
                                // Save codes for current key
                                var key = $scope.buttonelem;
                                delete(key.spec);
                                delete(key.tcode);
                                _.extend(key, codes);
                                showPrompt('Key learnt successfully.');
                                $scope.dirty = true;
                                $timeout($scope.stopLearning, 1000);
                            }
                        }
                    })
                    .catch(function(error) {
                        console.log('*** readIrCode:', error);
                        $scope.stopLearning();
                    });
            };

            readIrCode();
        };

        $scope.stopLearning = function() {
            wid.cancelKeyPress($scope.widget);
            $scope.buttonelem.learning = false;
            $scope.buttonelem.analyzing = false;
            hidePrompt();
        }

        $scope.nameChange = function() {
            $scope.dirty = true;
        };

        $scope.iconChange = function() {
            $scope.dirty = true;
        };

        $scope.save = function() {
            if (!$scope.widget) return;
            // save and continue editing remote
            $scope.gadget.editremote.keys[$stateParams.buttonIndex] = $scope.buttonelem;
            $scope.gadget.editremote.dirty = true;
            $scope.dirty = false;
            $state.go(rdb.is.state + 'editgadget', {
                widgetId: $stateParams.widgetId,
                gadgetId: $stateParams.gadgetId
            });
            return;
        };

        $scope.deleteButton = function() {
            var key = $scope.gadget.editremote.keys[$stateParams.buttonIndex];
            var kindex = _.findIndex($scope.gadget.editremote.layout, function(k) {
                return k === key.key;
            })
            if (kindex >= 0) {
                $scope.gadget.editremote.layout.splice(kindex,1);
            }
            $scope.gadget.editremote.keys.splice($stateParams.buttonIndex,1);
            $state.go(rdb.is.state + 'editgadget', {
                widgetId: $stateParams.widgetId,
                gadgetId: $stateParams.gadgetId
            });
        };

        autorouter();
    });
