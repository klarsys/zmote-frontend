angular.module('zMoteApp')
    .factory('wid', function($http, localStorageService, $interval, $timeout, $q, rdb, ApiBase) {
        // for client-widget communication
        var irwrite = function(widget, key) {
            if (!widget.queue) widget.queue = [];
            item = {};
            item.widget = widget;
            item.key = key;
            item.ts = Date.now();
            widget.queue.push(item);
            if (widget.queue.length == 1) // if no other request, kick
                irwrite_next(widget);
        };
        var init_widget = function (widget) {
            if (!widget.base) 
                return getsta_mac(widget);
            else
                return $q.when(true); 
        };
        var delay = function (n) {
            return $timeout(function () {
                return true;
            }, n);
        }
        var getMQTTResponse = function (widget, id, ntries) {
            if (!ntries)
                throw(new Error("MQTT response timed out"));
            return delay(500)
                .then(function () {
                    return $http.get(ApiBase+'/widgets/'+widget._id+'/command/'+id);
                })
                .then(function (resp) {
                    console.log("mqtt resp", resp.data);
                    if (resp.data.status == "pending")
                        return getMQTTResponse(widget, id, ntries - 1);
                    else
                        return resp;
                });
        };
        var sendCommand = function (widget, verb, url, payload, ntries) {
            if (typeof ntries === undefined)
                ntries = 4;
            else if (ntries === 0)
                throw(new Error("Command timeout"));
            console.log("sendCOmmand: base="+widget.base + url, payload);
            var chain;
            if (widget.local) {
                chain =  $http[verb](widget.base + url, payload);
            } else {
                // widget is outside of local network. Need to use MQTT gateway
                chain = $http[verb](ApiBase+'/widgets/'+widget._id+'/api/'+url, payload)
                    .then(function (resp) {
                        console.log("mqtt start", resp.data);
                        return getMQTTResponse(widget, resp.data._id, 4);
                    });
            }
            return chain
                .then(function (resp) {
                    var status = (resp.data && resp.data.status && resp.data.status.toLowerCase()) || "error";
                    console.log("status", status, resp);
                    if (status == "busy" || status == "wait")
                        return sendCommand(widget, verb, url, payload, ntries-1);
                    else if (status == "ok")
                        return resp;
                    else
                        throw(new Error("Bad command status: "+status));
                });
        };
        var irwrite_next = function(widget) {
            if (!widget.queue.length) // queue is empty
                return;
            var item = widget.queue[0];
            if (item.ts < Date.now() - 2000) {// FIXME arbitrary timeout for pending keypresses
                widget.queue.shift(); // FIXME. need to retry.
                irwrite_next(widget);
            }
            //var ip = (item.widget.localIP)?item.widget.localIP:item.widget.extIP;
            init_widget(widget)
                .then(function() {
                    var code = item.key.code;
                    if (item.key.tcode && item.key.tcode.seq.length > 0) { // tcode present
                        code = widget.toggle? item.key.tcode: item.key.code;
                        widget.toggle = !widget.toggle;
                    }
                    // Repeat atleast once
                    if (code.repeat[0] < 1)
                        code.repeat[0] = 1;
                    return sendCommand(widget, 'put', 'ir/write', code);
                })
                .then(function() {
                        console.log('irwrite done');
                        widget.queue.shift(); 
                        irwrite_next(widget);
                    },
                    function(err) {
                        console.log('err in irwrite', err);
                        irwrite_next(widget);
                    }
                );
        };

        var getsta_mac = function(widget) {
            console.log("get_mac", widget.extIP, rdb);
            if (!widget.local) { //if (widget.extIP != rdb.status.extIP) {
                widget.base = true;
                //widget.local = false;
                return $q.when(true); // No need to worry about connection status
            }
            //widget.local = true;
            var ip = widget.localIP;
            return $http.get('http://' + ip + '/api/wifi/mac')
                .then(
                    function(res) {
                        widget.sta_mac = res.data.sta_mac;
                        widget.base = 'http://' + widget.localIP + '/' 
                            + widget.sta_mac + '/api/';
                    },
                    function(err) {
                        console.log('/api/wifi/mac failed');
                        // FIXME: handle disconnection
                        throw(new Error("Disconnected:"+err));
                    });
        };

        var getKeyPress = function(widget) {
            widget.cancelKeyPress = false;
            console.log("getKeyPress", widget.base, widget.localIP);
            return init_widget(widget)
                .then(function() {
                    console.log("getKeyPress: try", widget.base, widget.localIP);
                    return sendCommand(widget, 'get', 'ir/trigger');
                })
                .then(function() {
                    var q = $q.defer();
                    var ntries = 0;

                    function loop() {
                        sendCommand(widget, 'get', 'ir/read')
                            .then(function(resp) {
                                if (widget.cancelKeyPress) {
                                    q.reject('Operation cancelled on request');
                                } else if (resp.data.trigger && resp.data.trigger.length > 30) { // Keep this number high to avoid false triggers
                                    q.resolve(resp.data);
                                } else {
                                    ++ntries;
                                    if (ntries >= 20000) { // Forever
                                        q.reject('Timed out waiting for key press');
                                    } else
                                        $timeout(loop, 1000);
                                }
                            }, function (err) {
                                console.log("Unexpected error in learning");
                                q.reject(err);
                            });
                    }
                    loop();
                    return q.promise;
                })
                .catch(function(error) {
                    console.log('*** getKeyPress:', error);
                    return false;
                });
        }

        var cancelKeyPress = function(widget) {
            widget.cancelKeyPress = true;
        }

        return {
            irwrite: irwrite,
            getKeyPress: getKeyPress,
            cancelKeyPress: cancelKeyPress
        };
    });

