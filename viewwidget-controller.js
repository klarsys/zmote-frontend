angular.module('zMoteApp')
    .controller("ViewWidgetController", function($scope, $location, $timeout, $state, $stateParams, localStorageService, rdb, $mdDialog, $http) {
        $scope.hint = "fetching remotes...";
        $scope.palettes = rdb.palettes;

        var autorouter = function() {
            if (!rdb.status.widgetsUpdated) {
                $timeout(autorouter, 500);
                return;
            }
            var windex = _.findIndex(rdb.w.widgets, function(widget) {
                return widget._id === $stateParams.widgetId;
            });
            if (windex < 0) { // widget not found. goto widgets
                $state.go(rdb.is.state + 'widgets');
                return;
            }
            $scope.widget = rdb.w.widgets[windex];
            gadgetAutoroute($scope.widget);
        };

        var gadgetAutoroute = function(widget) {
            if (!widget.gadgetsUpdated) {
                $timeout(function() {
                    gadgetAutoroute(widget);
                }, 500);
                return;
            }
            /*if (widget.gadgets.length == 1) { // only one widget. go there
                $state.go(rdb.is.state + 'gadget', {
                    widgetId: widget._id,
                    gadgetId: widget.gadgets[0]._id
                });
            }
            if (widget.gadgets.length == 0) { // no gadgets. go to addgadget page
                $state.go(rdb.is.state + 'addgadget', {
                    widgetId: widget._id
                });
                return;
            }*/
            rdb.checkConnectedFlag(widget); // kick the checking loop
            widget.beenToViewWidget = true;
        };

        $scope.goBack = function() {
            $state.go(rdb.is.state + 'widgets', {
                widgetId: $scope.widget._id
            });
        };

        $scope.deleteGadget = function(event, gadgetdata) {
            rdb.deleteGadget($scope.widget, gadgetdata);
        };

        $scope.addGadget = function(ev) {
            $state.go(rdb.is.state + "addgadget", {
                widgetId: $stateParams.widgetId
            });
        };

        $scope.viewGadget = function(ev, gadget) {
            $state.go(rdb.is.state + "gadget", {
                widgetId: $stateParams.widgetId,
                gadgetId: gadget._id
            });
        };

        $scope.menuClick = function(menuitem) {
            if (menuitem.func) menuitem.func(menuitem);
        };

        $scope.gotoConfigure = function() {
            $state.go(rdb.is.state + 'configure', {
                widgetId: $scope.widget._id
            });
        };

        if (rdb.is.state == 'demo.') {
            $scope.menuitems = [];
        } else {
            $scope.menuitems = [{
                name: "Configure zMote",
                func: $scope.gotoConfigure
            }];
        }

        $scope.$on("$destroy", function(event) {
            rdb.checkConnectedFlag(); // to cancel the checking loop
        });

        autorouter();
    });
