angular
    .module('zMoteApp')
    .controller('IRToolController', ['$scope', '$state', '$stateParams', '$http', '$timeout', '$mdToast', 'rdb', 'wid', 'ApiBase', 'localStorageService', function($scope, $state, $stateParams, $http, $timeout, $mdToast, rdb, wid, ApiBase, localStorageService) {

        var timer = null;
        $scope.$on("$destroy", function(event) {
            $scope.stopLearning();
            hideBusy();
            $timeout.cancel(timer);
            timer = null;
        });

        $scope.reset = function() {
            $scope.stamac = null;
            $scope.error = '';
            $scope.local = false;
            $scope.buttonelem = {
                learning: false,
                analyzing: false
            };
            $scope.spec = {};
        }

        $scope.goBack = function() {
            $state.go('home');
        };

        var get = function(url, config) {
            // console.log('GET', url);
            return $http.get(url, config);
        };

        var put = function(url, data, config) {
            // console.log('PUT', url);
            // console.log(data);
            return $http.put(url, data, config);
        };

        var post = function(url, data, config) {
            // console.log('POST', url);
            // console.log(data);
            return $http.post(url, data, config);
        };

        var toastVisible = false;
        var showBusy = function(message) {
            if (toastVisible)
                $mdToast.updateContent(message);
            else {
                toastVisible = true;
                $mdToast.show($mdToast.simple().content(message).hideDelay(0));
            }
        };
        var hideBusy = function() {
            $mdToast.hide();
            toastVisible = false;
        };

        $scope.learn = function() {
            if ($scope.buttonelem.learning)
                $scope.stopLearning();
            else
                $scope.startLearning();
        };

        $scope.startLearning = function() {
            var readIrCode = function() {
                $scope.buttonelem.learning = true;
                showBusy('Press key on your real remote');
                wid.getKeyPress($scope.widget)
                    .then(function(data) {
                        if (data) {
                            $scope.buttonelem.learning = false;
                            $scope.buttonelem.analyzing = true;
                            showBusy('Analyzing...');
                            return rdb.getKeyCodes(data);
                        }
                        // Else, cancelled
                    })
                    .then(function(codes) {
                        $scope.buttonelem.analyzing = false;
                        if (codes) {
                            var key = $scope.buttonelem;
                            delete(key.spec);
                            delete(key.tcode);
                            _.extend(key, codes);
                            if (codes.spec !== undefined)
                                $scope.spec = codes.spec;
                            else
                                $scope.spec = {
                                    protocol: '',
                                    device: '',
                                    subdevice: '',
                                    obc: '',
                                    misc: ''
                                }
                            $scope.stopLearning();
                        }
                    })
                    .catch(function(error) {
                        console.log('*** readIrCode:', error);
                        $scope.stopLearning();
                    });
            };

            readIrCode();
        };

        $scope.stopLearning = function() {
            wid.cancelKeyPress($scope.widget);
            $scope.buttonelem.learning = false;
            $scope.buttonelem.analyzing = false;
            hideBusy();
        }

        var irEncode = function(spec) {
            var toggleBitPresent = false;
            var codes = {
                confidence: 0
            };

            return $http.post(ApiBase + '/irp/encode', spec)
                .then(function(resp) {
                    if (resp.data.error) {
                        return codes;
                    } else {
                        codes.confidence += 16; // FIXME
                        codes.code = resp.data;
                        if (toggleBitPresent) {
                            codes.spec.misc = 'T=1';
                            return $http.post(ApiBase + '/irp/encode', codes.spec)
                                .then(function(resp) {
                                    codes.tcode = resp.data;
                                    return codes;
                                });
                        } else {
                            return codes;
                        }
                    }
                })
                .catch(function(error) {
                    console.log('*** Error in irEncode:', error);
                    return false;
                });
        };

        $scope.send = function() {
            showBusy('Encoding...');
            irEncode($scope.spec)
                .then(function(codes){
                    console.log(codes);
                    var key = {
                        code: codes.code
                    };
                    wid.irwrite($scope.widget, key);
                    hideBusy();
                }, function(){
                    hideBusy();
                });
        };
        $scope.prev = function() {
            delete($scope.key);
            $scope.spec.obc--;
            $scope.send();
        };
        $scope.next = function() {
            delete($scope.key);
            $scope.spec.obc++;
            $scope.send();
        };

        var connectToWidget = function() {
            showBusy('Connecting...');
            get('http://' + $scope.widget.localIP + '/api/wifi/mac', {
                    timeout: 3000
                })
                .then(function(response) {
                    $scope.error = '';
                    $scope.stamac = response.data.sta_mac;
                    chipID = '00' + $scope.stamac.replace(/-/g, '').substring(6, 12)
                    if (chipID === $scope.widget.chipID)
                        $scope.local = true;
                    hideBusy();
                }, function(error) {
                    // FIXME: MQTT
                    $scope.error = 'Could not establish connection';
                    console.log('Configure:', error);
                    hideBusy();
                    if (timer) timer = $timeout(connectToWidget, 3000); // Hack
                });
        };

        $scope.searchAgain = function() {
            $scope.reset();
            connectToWidget();
        };

        var getWidget = function() {
            $scope.widget = null;
            $scope.reset();

            var windex = _.findIndex(rdb.w.widgets, function(widget) {
                return widget._id == $stateParams.widgetId;
            })
            if (windex >= 0) {
                $scope.widget = rdb.w.widgets[windex];
                $scope.searchAgain();
            } else
                timer = $timeout(getWidget, 500);
        };

        showBusy('Loading...');
        timer = $timeout(getWidget, 0); // Hack
    }]);
