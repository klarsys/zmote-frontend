angular.module('zMoteApp')
    .controller("DemoBadgeController", function($scope, $window) {
        $scope.is = {};
        $scope.is.demo = $window.isDemo;

        $scope.$watch(function() {
            return $window.isDemo;
        }, function() {
            $scope.is.demo = $window.isDemo;
        });

    });
