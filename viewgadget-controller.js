angular.module('zMoteApp')
    .controller("ViewGadgetController", function($scope, $location, $timeout, $state, $stateParams, localStorageService, rdb, $mdDialog, $http, wid, $mdToast) {
        //$scope.remotes = rdb.remotes;
        $scope.intrenable = false;
        $scope.palettes = rdb.palettes;
        $scope.hues = rdb.hues;
        $scope.showPrevPage = false;
        $scope.showNextPage = false;
        $scope.gadget = {};
        $scope.remote = {};
        $scope.hint = "loading...";
        $scope.widget = {};
        $scope.windex = -1;
        $scope.gindex = -1;
        $scope.selectedGadget = -1;
        $scope.enableDownload = false;
        if ($location.search().download && $location.search().download == "true") {
            $scope.enableDownload = true;
            $scope.downloadName = "remote.json";
        }

        var autorouter = function() {
            if (!rdb.status.widgetsUpdated) {
                $timeout(autorouter, 500);
                return;
            }
            var windex = _.findIndex(rdb.w.widgets, function(widget) {
                return widget._id === $stateParams.widgetId;
            });
            if (windex < 0) { // widget not found. goto widgets
                $state.go(rdb.is.state + 'widgets');
                return;
            }
            $scope.widget = rdb.w.widgets[windex];
            gadgetAutoroute($scope.widget);
        };

        var gadgetAutoroute = function(widget) {
            if (!widget.gadgetsUpdated) {
                $timeout(function() {
                    gadgetAutoroute(widget);
                }, 500);
                return;
            }
            $scope.gindex = _.findIndex(widget.gadgets, function(gadget) {
                return gadget._id === $stateParams.gadgetId;
            });
            if ($scope.gindex < 0) { // gadget not found. go back
                $state.go(rdb.is.state + 'widget', {
                    widgetId: widget._id
                });
                return;
            }
            $scope.gadget = $scope.widget.gadgets[$scope.gindex];
            $scope.remote = $scope.gadget.userRemote ? $scope.gadget.userRemote : $scope.gadget.remote;
            if ($scope.enableDownload) {
                $scope.download = "data:application/json;base64," + btoa(JSON.stringify($scope.remote, null, " "));
                $scope.downloadName = $scope.remote.brand + '-' + $scope.remote.model + '-' + $scope.remote.name + '.json';
            }
            $scope.buttongroup = rdb.makeButtonGroup($scope.remote);
            if ($scope.gadget.editremote) delete($scope.gadget.editremote); // discard any editingchanges
            $timeout(function() {
                $scope.updatePage('init');
            }, 500);

            $scope.selectedGadget = $scope.gindex;
            $scope.palette = $scope.palettes[$scope.selectedGadget % $scope.palettes.length];
            rdb.checkConnectedFlag(widget); // kick the loop
        };

        $scope.goBack = function() {
            $state.go(rdb.is.state + 'widget', {
                widgetId: $stateParams.widgetId
            });
        };

        $scope.goHome = function() {
            $state.go('home');
        };

        $scope.data = {
            selectedIndex: 0,
        };
        $scope.next = function() {
            $scope.data.selectedIndex = Math.min($scope.data.selectedIndex + 1, $scope.buttongroup.length - 1);
        };
        $scope.previous = function() {
            $scope.data.selectedIndex = Math.max($scope.data.selectedIndex - 1, 0);
        };
        $scope.updatePage = function(action) {
            if (action === "init") $scope.data.selectedIndex = 0;
            if (action === "next") $scope.next();
            if (action === "prev") $scope.previous();
            // show appropriate page buttons
            $scope.showPrevPage = false;
            $scope.showNextPage = false;
            if ($scope.data.selectedIndex > 0) $scope.showPrevPage = true;
            if ($scope.data.selectedIndex < ($scope.buttongroup.length - 1)) $scope.showNextPage = true;
        };

        $scope.gadgetSelect = function(index) {
            // md-theme-watch not working on toolbar. so, for now, using state.go when remote changes
            $state.go(rdb.is.state + "gadget", {
                widgetId: $scope.widget._id,
                gadgetId: $scope.widget.gadgets[index]._id
            });
        };

        $scope.gadgetPrev = function() {
            if ($scope.selectedGadget > 0)
                $scope.gadgetSelect($scope.selectedGadget - 1);
        };
        $scope.gadgetNext = function() {
            if ($scope.selectedGadget < $scope.widget.gadgets.length - 1)
                $scope.gadgetSelect($scope.selectedGadget + 1);
        };

        $scope.sendCode = function(belem) {
            wid.irwrite($scope.widget, belem);
        };
        $scope.menuClick = function(menuitem) {
            if (menuitem.func) menuitem.func(menuitem);
        };

        $scope.editGadget = function() {
            $state.go(rdb.is.state + "editgadget", {
                widgetId: $stateParams.widgetId,
                gadgetId: $stateParams.gadgetId
            });
        };

        /*
        $scope.enableDownloadGadget = function() {
            $state.enableDownload = true;
            $state.go(rdb.is.state + "enableDownloadgadget", {
                widgetId: $stateParams.widgetId,
                gadgetId: $stateParams.gadgetId
            });
        };
        */

        $scope.deleteGadget = function(ev) {
            var confirm = $mdDialog.confirm()
                .title('Delete this Remote?')
                .content('')
                .ariaLabel('Delete remote')
                .targetEvent(ev)
                .ok('Delete')
                .cancel('Cancel')
                .clickOutsideToClose(true)
                .escapeToClose(true);
            $mdDialog.show(confirm).then(function() {
                $mdDialog.hide();
                $mdDialog.show($mdDialog.confirm()
                    .title('Deleting this Remote..')
                    .content('')
                    .ariaLabel('Delete remote')
                    .clickOutsideToClose(true)
                    .escapeToClose(true));
                rdb.deleteGadget($scope.widget, $scope.gadget).then(
                    function() {
                        $state.go(rdb.is.state + "widget", {
                            widgetId: $stateParams.widgetId
                        });
                        return true;
                    },
                    function(err) {
                        console.log("err in deleteGadget", err);
                        return false;
                    }).then(function() {
                    $mdDialog.hide();
                });
            });
        };

        $scope.menuitems = [{
                name: "Edit Remote",
                func: $scope.editGadget
            },
            /*
                    {
                        name: "Download Remote",
                        func: $scope.enableDownloadGadget
                    },*/
            {
                name: "Delete Remote",
                func: $scope.deleteGadget
            }, {
                name: "Back",
                func: $scope.goBack
            }
        ];

        $scope.$on("$destroy", function(event) {
            $mdDialog.hide();
            rdb.checkConnectedFlag(); // to cancel the checking loop
        });

        autorouter();
    });
