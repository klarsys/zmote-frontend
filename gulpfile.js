var gulp = require('gulp'),
    gulpif = require('gulp-if'),
    jshint = require('gulp-jshint'),
    concat = require('gulp-concat'),
    uglify = require('gulp-uglify'),
    rename = require('gulp-rename'),
    useref = require('gulp-useref'),
    minifyCss = require('gulp-minify-css'),
    ngAnnotate = require('gulp-ng-annotate'),
    request = require('sync-request'),
    replace = require('gulp-replace'),
    modify = require('gulp-modify'),
    del = require('del');
var browserSync = require('browser-sync').create();
var inline_font = true;
var gulpUtil = require('gulp-util');
// Lint Task
gulp.task('lint', function() {
    return gulp.src('*.js')
        .pipe(jshint())
        .pipe(jshint.reporter('default'));
});

// Concatenate & Minify JS
gulp.task('server-app', function() {
    gulp.src(['*.html'])
        .pipe(useref())
        .pipe(gulpif('*.js', ngAnnotate()))
        .pipe(gulpif('*.js', uglify()))
        .pipe(gulpif('*.css', minifyCss()))
        .pipe(useref())
        .pipe(gulp.dest('../public/app'))
        .pipe(browserSync.stream());
    gulp.src('favicon.ico')
        .pipe(gulp.dest('../public/app'));
});

function mkpage(pg) {
    var base = pg.replace(/.*\//, '');
    gulp.src(["landing/_header.html", pg, "landing/_footer.html"])
        .pipe(concat(base))
        .pipe(useref())
        .pipe(gulpif('*.js', ngAnnotate()))
        .pipe(gulpif('*.js', uglify()))
        .pipe(gulpif('*.css', minifyCss()))
        .pipe(useref())
        .pipe(gulp.dest('../public'))
        .pipe(browserSync.stream());
}
gulp.task('server-landing', function() {
    mkpage("landing/landing.html");
    mkpage("landing/tos.html");
    mkpage("landing/privacy.html");
    mkpage("landing/buy.html");
    gulp.src("threejs/*").pipe(gulp.dest('../public/threejs'))
        .pipe(browserSync.stream());
    gulp.src("p2p/*").pipe(gulp.dest('../public/p2p'))
        .pipe(browserSync.stream());
    gulp.src([
        "landing/index.html",
        "landing/flat-ui/css/flat-ui.css",
        "landing/flat-ui/fonts/*",
        "landing/img/*",
        "landing/common-files/icons/menu-icon.png",
        "landing/fonts/*",
        "landing/video/*",
        "landing/common-files/fonts/Startup-Icons.woff",
        "landing/common-files/fonts/Startup-Icons.ttf",
        "landing/common-files/fonts/Flat-UI-Icons.woff",
        "landing/common-files/fonts/Flat-UI-Icons.ttf"], {base : 'landing/'})
        .pipe(gulp.dest('../public'))
        .pipe(browserSync.stream());
    gulp.src(['favicon.ico','image.jpg', "landing/img/zmote-v2.png"])
        .pipe(gulp.dest('../public'));
});

gulp.task('server-files', ['server-app', 'server-landing']);
// Static server
gulp.task('browser-sync-init', function() {
    browserSync.init({
        server: {
            baseDir: "../public"
        }
    });
});
gulp.task('watch', ['browser-sync-init', 'server-files'], function() {
  gulp.watch(['*.html', '*.js', '*.css'], ['server-app']);
  gulp.watch(['landing/**/*.html', 'landing/**/*.css','landing/**/*.js'], ['server-landing']);
});

// Widget related from here
function inlineSources(m) {
    var headers = {headers: {'User-Agent': 'Mozilla/5.0 (Windows NT 6.3; Trident/7.0; rv:11.0) like Gecko'}};
            if (!m.match(/(href|src)="(https?:)?\/\//)) {
                //console.log("Ignoring: "+m);
                return m;
            }
            var link = 'http://' + m.match(/(href|src)="(https?:)?\/\/(.*?)"/)[3];
            console.log("Downloading "+link+"...");
            var content = request('GET', link, headers).body.toString('utf-8');
            if (!content) {
                console.error("Fail!");
                return m;
            } else if (m.match(/link[^>]*rel="stylesheet"/)) {
                if (!inline_font)
                    return '\n<style type="text/css">\n'+content+'\n</style>\n';
                var inContent = content.split('\n').map(function (line) {
                    if (!line.match(/url\(https?:\/\/.*?\)/))
                        return line;
                    var url = "http://"+line.match(/url\(https?:\/\/(.*?)\)/)[1];
                    console.log("2nd level get "+url);
                    var font = request('GET', url, headers)
                        .body.toString('base64');
                    //console.log(request('GET', link, headers).body.toString());
                    //var font = btoa(request('GET', link, headers).body.toString());
                    //var font = (new Buffer(request('GET', link, headers).body.toString())).toString('base64');
                    return line.replace(/url\(https?:\/\/.*?\)/,'url(\'data:application/x-font-woff;base64,'+font+'\')' );
                }).join('\n');
                return '\n<style type="text/css">\n'+inContent+'\n</style>\n';
            } else if (m.match(/<script/))
                return '\n<script type="text/javascript">\n'+content+'\n</script>\n';

}

gulp.task('widget-main', function () {
    //var assets = useref.assets();
    return gulp.src(['miniwidget/*.html'])
        .pipe(useref())
        .pipe(gulpif('*.js', uglify().on('error', gulpUtil.log)))
        .pipe(gulpif('*.css', minifyCss()))
        //.pipe(assets.restore())
        .pipe(useref())
        .pipe(replace(/<link[^>]*rel="stylesheet".*?>/g, inlineSources))
        .pipe(replace(/<script.*?>(\s*<\/script>)?/g, inlineSources))
        .pipe(gulp.dest('../html'));
    });
gulp.task('widget-files', ['widget-main'], function () {
    return gulp.src(['favicon.ico'])
        .pipe(gulp.dest('../html'));
});