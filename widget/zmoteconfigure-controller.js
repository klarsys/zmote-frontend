angular
    .module('zMoteWidgetApp')
    .controller('ZMoteConfigureController', function($scope, $http, $timeout, $window, $mdToast) {
        console.log("COntroller");
        $scope.reset = function() {
            $scope.base = '/';
            //$scope.base = 'http://192.168.0.103/';
            $scope.stamac = null;
            $scope.info = null;
            $scope.name = null;
            $scope.networks = [];
            $scope.ssid = null;
            $scope.password = '';
            $scope.error = '';
        }

        $scope.searchAgain = function() {
            $scope.reset();
            connectToWidget();
        };

        $scope.saveName = function() {
            showBusy('Saving...');
            $http.post($scope.base + $scope.stamac + '/api/wifi/config', {
                    ssid: $scope.name
                })
                .then(function(response) {
                    // console.log(response.data);
                    $scope.configNameForm.$setPristine()
                    timer = $timeout($scope.searchAgain, 2000);
                })
                .catch(function(error) {
                    // FIXME
                    console.log('Configure:', error);
                    hideBusy();
                });
        };

        $scope.selectNetwork = function(n) {
            $scope.networks.forEach(function (nn) {
                nn.show = false;
            });
            n.show = true;
        };
        $scope.saveWifi = function(network) {
            showBusy('Saving...');
            $http.post($scope.base + $scope.stamac + '/api/wifi/connect', {
                    ssid: network.ssid,
                    password: network.password
                })
                .then(function(response) {
                    // console.log(response.data);
                    $scope.reset();
                    $scope.configWifiForm.$setPristine()
                    showBusy('Configuring Wifi network...');
                    // FIXME: Refresh widgets
                    timer = $timeout(function() {
                        showBusy('Connecting to new network...');
                        checkConnection();
                    }, 2000);
                })
                .catch(function(error) {
                    // FIXME
                    console.log('save Wifi err:', error);
                    hideBusy();
                });
        };
        var checkConnection = function() {
            $http.get($scope.base + 'api/wifi/mac')
                .then(function() {
                    // Still connected to the hotspot.  Try after a while
                    $timeout(checkConnection, 500);
                }, function() {
                    // AP disconnected
                    //$window.location.reload();
                    $window.location.href = "http://www.zmote.io/app";
                })
        };

        var toastVisible = false;
        var showBusy = function(message) {
            if (toastVisible)
                $mdToast.updateContent(message);
            else {
                toastVisible = true;
                $mdToast.show($mdToast.simple().content(message).hideDelay(0));
            }
        };
        var hideBusy = function() {
            $mdToast.hide();
            toastVisible = false;
        };

        var getWiFiNetworks = function() {
            showBusy('Scanning Wi-Fi networks...')
            $http.get($scope.base + $scope.stamac + '/api/wifi/scan')
                .then(function(response) {
                    $scope.networks = response.data;
                    for (var i = $scope.networks.length - 1; i >= 0; i--) {
                        if ($scope.networks[i].connected === true)
                            $scope.ssid = $scope.networks[i].ssid;
                    };
                    hideBusy();
                })
                .catch(function(error) {
                    // FIXME: MQTT
                    console.log('getWifi error:', error, $scope.base + $scope.stamac + '/api/wifi/scan');
                    hideBusy();
                })
        };

        var getWiFiStatus = function() {
            showBusy('Reading configuration...')
            $http.get($scope.base + $scope.stamac + '/api/wifi/status')
                .then(function(response) {
                    $scope.info = response.data;
                    $scope.name = $scope.info.ap_ssid;
                    getWiFiNetworks();
                })
                .catch(function(error) {
                    // FIXME: MQTT
                    console.log('getWifi Status error:', error);
                    hideBusy();
                })
        };

        var connectToWidget = function() {
            showBusy('Connecting...');
            $http.get($scope.base + 'api/wifi/mac')
                .then(function(response) {
                    $scope.error = '';
                    $scope.stamac = response.data.sta_mac;
                    chipID = '00' + $scope.stamac.replace(/-/g, '').substring(6, 12);
                    getWiFiStatus();
                }, function(error) {
                    $scope.error = 'Could not establish connection';
                    console.log('connect error:', error, $scope.base + 'api/wifi/mac');
                    showBusy('Error connecting!');
                    if (timer) timer = $timeout(connectToWidget, 1000); // Hack
                });
        };


        showBusy('Loading...');
        $scope.reset();
        timer = $timeout(connectToWidget, 0); // Hack
    });
