angular
    .module('zMoteWidgetApp', ['ngMaterial'])
    .config(function($mdThemingProvider) {
        var palettes = ["red", "pink", "purple", "deep-purple", "indigo", "blue", "light-blue", "cyan", "teal", "green", "light-green", "lime", "yellow", "amber", "orange", "deep-orange", "brown", "grey", "blue-grey"]; //
        //var palettes = ["blue","light-blue","cyan"];

        // this is not working. for now just using md-hue-1,2,3
        var hues = {
            'hue-50': '50', // use shade 50 for the <code>md-hue-50</code> class
            'hue-100': '100', // use shade 100 for the <code>md-hue-100</code> class
            'hue-200': '200', // use shade 200 for the <code>md-hue-200</code> class
            'hue-300': '300',
            'hue-400': '400',
            'hue-500': '500',
            'hue-600': '600',
            'hue-700': '700',
            'hue-800': '800',
            'hue-900': '900',
            'hue-A100': 'A100',
            'hue-A200': 'A200',
            'hue-A400': 'A400',
            'hue-A700': 'A700',
        };
        if (true) {
            palettes.forEach(function(palette) {
                $mdThemingProvider.theme(palette)
                    .primaryPalette(palette /*,hues*/ ); // specify primary color all
                // other color intentions will be inherited
                // from default
            })
        }
        $mdThemingProvider.theme('editTheme')
            .primaryPalette('deep-orange'); // specify primary color all

    })
    .config([
        '$compileProvider',
        function($compileProvider) {
            $compileProvider.aHrefSanitizationWhitelist(/^\s*(https?|mailto|chrome-extension|intent|com.farlens.flockr|whatsapp):/);
        }
    ]);
