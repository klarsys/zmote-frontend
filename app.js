angular
    .module('zMoteApp', ['ngMaterial', 'ui.router', 'LocalStorageModule', 'ngCookies','cgNotify'])
    .constant('ApiBase', '//v1.zmote.io')
    //.constant('ApiBase', '//192.168.0.106:3000')
    .config(function($stateProvider, $urlRouterProvider, $httpProvider) {
        $httpProvider.defaults.withCredentials = true;

        $urlRouterProvider.otherwise("/app/widgets");
        $stateProvider
            .state('home', {
                url: "/app",
                templateUrl: "app.html"
            })
            .state('demo', {
                url: "/demo",
                templateUrl: "app.html"
            })
            .state('app', {
                url: "/app",
                templateUrl: "app.html"
            })
            .state('app.widgets', {
                url: "/widgets",
                templateUrl: "widgets.html"
            })
            .state('demo.widgets', {
                url: "/widgets",
                templateUrl: "widgets.html"
            })
            .state('app.widget', {
                url: "/widget/{widgetId}/view",
                templateUrl: "viewwidget.html"
            })
            .state('app.update', {
                url: "/widget/{widgetId}/update",
                templateUrl: "updatewidget.html"
            })
            .state('demo.widget', {
                url: "/widget/{widgetId}/view",
                templateUrl: "viewwidget.html"
            })
            .state('app.gadget', {
                url: "/widget/{widgetId}/gadget/{gadgetId}/view",
                templateUrl: "viewgadget.html"
            })
            .state('demo.gadget', {
                url: "/widget/{widgetId}/gadget/{gadgetId}/view",
                templateUrl: "viewgadget.html"
            })
            .state('app.addgadget', {
                url: "/widget/{widgetId}/add",
                templateUrl: "addgadget.html"
            })
            .state('demo.addgadget', {
                url: "/widget/{widgetId}/add",
                templateUrl: "addgadget.html"
            })
            .state('app.editgadget', {
                url: "/widget/{widgetId}/gadget/{gadgetId}/edit",
                templateUrl: "editgadget.html"
            })
            .state('demo.editgadget', {
                url: "/widget/{widgetId}/gadget/{gadgetId}/edit",
                templateUrl: "editgadget.html"
            })
            .state('app.editgadget.addbuttons', {
                url: "/addbuttons",
                parent: 'app.editgadget',
                templateUrl: "addbuttons.html"
            })
            .state('demo.editgadget.addbuttons', {
                url: "/addbuttons",
                parent: 'demo.editgadget',
                templateUrl: "addbuttons.html"
            })
            .state('app.editbutton', {
                url: "/widget/{widgetId}/gadget/{gadgetId}/button/{buttonIndex}/edit",
                templateUrl: "editbutton.html"
            })
            .state('demo.editbutton', {
                url: "/widget/{widgetId}/gadget/{gadgetId}/button/{buttonIndex}/edit",
                templateUrl: "editbutton.html"
            })
            .state('app.configure', {
                url: '/configure/{widgetId}',
                templateUrl: "zmoteconfigure.html"
            })
            .state('app.irtool', {
                url: '/irtool/{widgetId}',
                templateUrl: "irtool.html"
            })
    })
    .config(function($mdThemingProvider) {
        var palettes = ["red", "pink", "purple", "deep-purple", "indigo", "blue", "light-blue", "cyan", "teal", "green", "light-green", "lime", "yellow", "amber", "orange", "deep-orange", "brown", "grey", "blue-grey"]; //
        //var palettes = ["blue","light-blue","cyan"];

        // this is not working. for now just using md-hue-1,2,3
        var hues = {
            'hue-50': '50', // use shade 50 for the <code>md-hue-50</code> class
            'hue-100': '100', // use shade 100 for the <code>md-hue-100</code> class
            'hue-200': '200', // use shade 200 for the <code>md-hue-200</code> class
            'hue-300': '300',
            'hue-400': '400',
            'hue-500': '500',
            'hue-600': '600',
            'hue-700': '700',
            'hue-800': '800',
            'hue-900': '900',
            'hue-A100': 'A100',
            'hue-A200': 'A200',
            'hue-A400': 'A400',
            'hue-A700': 'A700',
        };
        if (true) {
            palettes.forEach(function(palette) {
                $mdThemingProvider.theme(palette)
                    .primaryPalette(palette /*,hues*/ ); // specify primary color all
                // other color intentions will be inherited
                // from default
            })
        }
        $mdThemingProvider.theme('editTheme')
            .primaryPalette('deep-orange'); // specify primary color all

    })
    .config([
        '$compileProvider',
        function($compileProvider) {
            $compileProvider.aHrefSanitizationWhitelist(/^\s*(https?|mailto|chrome-extension|intent|com.farlens.flockr|whatsapp|data):/);
        }
    ]);
