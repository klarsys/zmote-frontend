angular.module('zMoteApp')
    .controller("UpdateWidgetController", function($scope, $timeout, $state, $stateParams, localStorageService, rdb, $q) {
        $scope.hint = "Loading...";
        $scope.updateState = "loading";
        $scope.updateMsg = "Starting...";
        var startUpdate = function() {
            if (!rdb.status.widgetsUpdated) {
                console.log(rdb.status);
                $timeout(startUpdate, 500);
                return;
            }
            $scope.widget = _.find(rdb.w.widgets, function(widget) {
                return widget._id === $stateParams.widgetId;
            });
            if (!$scope.widget)
                $scope.updateState = "error";
            else {
                $scope.updateState = "ongoing";
            }
            console.log("state=" + $scope.updateState);
            if ($scope.widget.fsUpdate) {
                $scope.updateMsg = "Updating filesystem...";
                return rdb.updateFS($scope.widget)
                    .then(function() {
                        if ($scope.widget.fwUpdate) {
                            $scope.updateMsg = "Updating firmware...";
                            return rdb.updateFW($scope.widget);
                        }
                    });
            } else if ($scope.widget.fwUpdate) {
                $scope.updateMsg = "Updating firmware...";
                return rdb.updateFW($scope.widget);
            } else {
                $scope.updateMsg = "Nothing to update";
                return $q.when(true);
            }
        };

        if (rdb.is.state == 'demo.') {
            $scope.menuitems = [];
        } else {
            $scope.menuitems = [{
                name: "Configure zMote",
                func: $scope.gotoConfigure
            }];
        }

        $scope.retry = function() {
            $scope.updateState = "loading";
            $scope.updateMsg = "Starting...";
            startUpdate().then(function() {
                $scope.updateState = "finished";
            }, function(err) {
                console.log("Update error");
                $scope.updateState = "error";
            });
        };
        $scope.abort = function() {
            $state.go(rdb.is.state + 'widgets');
        };
        $scope.finished = function() {
            $state.go(rdb.is.state + 'widget', {
                widgetId: $stateParams.widgetId
            });
        };
        $scope.retry();
    });
