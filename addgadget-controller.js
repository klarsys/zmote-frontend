angular.module('zMoteApp')
    .controller("AddGadgetController", function($scope, $timeout, $http, $state, $stateParams, rdb, $mdToast,$mdDialog) {
        $scope.remotes = [];
        $scope.project = {
            searchText: ""
        };
        $scope.selectedRemote = null;
        $scope.hint = "Search by Brand or Model Number";
        $scope.palettes = rdb.palettes;

        $scope.$on("$destroy", function(event) {
            $mdDialog.hide();
        });

        var autorouter = function() {
            if (!rdb.status.widgetsUpdated) {
                $timeout(autorouter, 500);
                return;
            }
            var windex = _.findIndex(rdb.w.widgets, function(widget) {
                return widget._id === $stateParams.widgetId;
            });
            if (windex < 0) { // widget not found. goto widgets
                $state.go(rdb.is.state + 'widgets');
                return;
            }
            $scope.widget = rdb.w.widgets[windex];
            $scope.widget.beenToAddGadget = true;
        };


        $scope.goBack = function() {
            $state.go(rdb.is.state+'widget', {
                widgetId: $stateParams.widgetId
            });
        };

        $scope.searchTextChange = _.debounce(function() {
            console.log($scope.project);
            $scope.remotes = [];
            $scope.selectedRemote = null;
            if ($scope.project.searchText && $scope.project.searchText.length >= 2) {
                // search
                $scope.hint = "Searching....";
                rdb.getRemotes($scope.project.searchText) // FIXME. not passing type for now
                    .then(function(remotes) {
                        $scope.remotes = remotes;
                        if ($scope.remotes.length > 0)
                            $scope.hint = "";
                        else
                            $scope.hint = "No remotes found";
                    }, function(res) {
                        console.log("err in getting remotes");
                    });
            } else {
                $scope.hint = "Search by Brand or Model Number";
            }
        }, 500, {
            leading: false,
            trailing: true
        });

        $scope.selectRemote = function(index) {
            console.log("selected ", index, $scope.remotes[index]);
            $scope.selectedRemote = $scope.remotes[index];
            $scope.addSelectedRemote();
        };
        $scope.addSelectedRemote = function() {
            var r = $scope.selectedRemote;
            var postObj = {
                client: {},
                name: "",
                remote: ""
            };
            $scope.selectedRemote = null;
            console.log("add selected remote", r);

            postObj.name = r.brand + ' ' + r.model;
            postObj.remote = r._id;

                $mdDialog.hide();
                $mdDialog.show($mdDialog.confirm()
                    .title('')
                    .content('Adding Remote...')
                    .ariaLabel('add remote'));
            rdb.addGadget($stateParams.widgetId, postObj).then(
                function(res) {
                    console.log("added successfully");
                    $state.go(rdb.is.state+'widget', {
                        widgetId: $stateParams.widgetId
                    });
                    return true;
                },
                function(res) {
                    console.log("failed to add");
                    $mdToast.show($mdToast.simple("failed to add remote"));
                    $state.go(rdb.is.state+'widget', {
                        widgetId: $stateParams.widgetId
                    });
                    return false;
                }).then(function(resp){
                    $mdDialog.hide();
                    return resp;
                });
        };
        autorouter();
    });
