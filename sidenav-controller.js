angular.module('zMoteApp')
    .controller("SidenavController", function($scope, $mdSidenav) {

        $scope.sidenavClick = function(menuitem) {
            $scope.menuClick(menuitem);
            $scope.toggle();
        };

        $scope.toggle = function() {
            $mdSidenav('right').toggle();
        };
    });
