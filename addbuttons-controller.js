angular.module('zMoteApp')
    .controller("AddButtonsController", function($scope, $mdDialog, rdb, $state, $stateParams, $mdToast) {
        // on page refresh or if this url is directly invoked, go to editgadget
        if (!$scope.$parent.comingFromEditMode) {
            $state.go(rdb.is.state+'editgadget', {
                widgetId: $stateParams.widgetId,
                gadgetId: $stateParams.gadgetId
            });
            return;
        }
        $scope.bgroup = rdb.makeButtonGroup($scope.$parent.remote);
        $scope.name = "Choose One Or More Buttons";
        $scope.show = {
            count: 0
        };
        $scope.buttonpage = [];

        $scope.goBack = function() {
            $state.go(rdb.is.state+'editgadget', {
                widgetId: $stateParams.widgetId,
                gadgetId: $stateParams.gadgetId
            });
        };

        $scope.highlightButton = function(event, buttonelem) {
            //if (event.type !== "click") return;
            buttonelem.highlight = !buttonelem.highlight;
            $scope.show.count = 0;
            $scope.bgroup.forEach(function(page) {
                page.forEach(function(brow) {
                    brow.forEach(function(belem) {
                        if (belem.highlight) $scope.show.count = $scope.show.count + 1;
                    })
                })
            })
            $scope.name = "Choose One Or More Buttons";
            if ($scope.show.count > 0) {
                $scope.name = "Add " + $scope.show.count + " Button";
                if ($scope.show.count > 1) $scope.name = $scope.name + "s";
            }
        };

        $scope.addButtons = function(event) {
            //if (event.type !== "click") return;
            //$mdDialog.hide($scope.bgroup);
            var count = 0;
            var newpage = [];
            // insert the button to last row
            $scope.bgroup.forEach(function(bpage) {
                bpage.forEach(function(brow) {
                    var newrow = [];
                    brow.forEach(function(belem) {
                        if (belem.highlight) {
                            var b = angular.copy(belem);
                            delete b.highlight;
                            delete belem.highlight;
                            newrow.push(b);
                            count = count + 1;
                        }
                    })
                    if (newrow.length > 0) newpage.push(newrow);
                })
            })
            var msg = "";
            if (newpage.length > 0) {
                $scope.$parent.buttongroup.splice(0, 0, newpage);
                $scope.$parent.remote.dirty = true;
                msg = "Added " + count + " buttons to the first page. Click and move rearrange.";
            } else {
                msg = "No buttons added";
            }
            $mdToast.show($mdToast.simple(msg));
            $scope.goBack();
        };

        $scope.$on("$destroy", function() {
            $scope.$parent.mode.edit = true;
        });

    });
