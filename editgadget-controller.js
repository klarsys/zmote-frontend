angular
    .module('zMoteApp')
    .controller("EditGadgetController", function($scope, $location, $timeout, $state, $stateParams, localStorageService, rdb, $mdDialog, $mdToast, wid) {
        $scope.palettes = rdb.palettes;
        $scope.hues = rdb.hues;
        $scope.markedButton = null;
        $scope.markedRow = null;
        $scope.markedPage = null;

        $scope.learning = false;
        $scope.mode = {};
        $scope.mode.edit = true; // to switch off when adding buttons
        $scope.mode.move = false;

        $scope.$on("$destroy", function(event) {
            // Cancel learning (also hides prompt)
            $scope.stopLearning();
            $mdDialog.hide();
            // FIXME: Prompt for saving if dirty
            //if ($scope.gadget && $scope.gadget.editremote) delete($scope.gadget.editremote);
            rdb.checkConnectedFlag(); // to cancel the checking loop
        });


        var autorouter = function() {
            if (!rdb.status.widgetsUpdated) {
                $timeout(autorouter, 500);
                return;
            }
            var windex = _.findIndex(rdb.w.widgets, function(widget) {
                return widget._id === $stateParams.widgetId;
            });
            if (windex < 0) { // widget not found. goto widgets
                $state.go(rdb.is.state + 'widgets');
                return;
            }
            $scope.widget = rdb.w.widgets[windex];
            gadgetAutoroute($scope.widget);
        };

        var gadgetAutoroute = function(widget) {
            if (!widget.gadgetsUpdated) {
                $timeout(function() {
                    gadgetAutoroute(widget);
                }, 500);
                return;
            }
            $scope.gindex = _.findIndex(widget.gadgets, function(gadget) {
                return gadget._id === $stateParams.gadgetId;
            });
            if ($scope.gindex < 0) { // gadget not found. go back
                $state.go(rdb.is.state + 'widget', {
                    widgetId: widget._id
                });
                return;
            }

            $scope.selectedGadget = $scope.gindex;

            $scope.gadget = $scope.widget.gadgets[$scope.gindex];
            if ($scope.gadget.editremote) {
                $scope.remote = $scope.gadget.editremote;
            } else {
                $scope.gadget.editremote = angular.copy($scope.gadget.userRemote ? $scope.gadget.userRemote : $scope.gadget.remote);
                $scope.remote = $scope.gadget.editremote;
            }
            $scope.buttongroup = rdb.makeButtonGroup($scope.remote);

            $scope.palette = $scope.palettes[$scope.selectedGadget % $scope.palettes.length];
            handleFooterButtons();
            removeMarks();
            rdb.checkConnectedFlag(widget); // kick the loop
        };

        $scope.goBack = function() {
            if ($scope.remote && $scope.remote.dirty) {
                var confirm = $mdDialog.confirm()
                    .title('Save changes to this Remote?')
                    .content('')
                    .ariaLabel('Save Changes')
                    .targetEvent(event)
                    .ok('Save')
                    .cancel('Discard')
                    .clickOutsideToClose(true)
                    .escapeToClose(true);
                $mdDialog.show(confirm).then(function() {
                    $mdDialog.hide();
                    $mdDialog.show($mdDialog.confirm()
                        .title('Saving...')
                        .content('')
                        .ariaLabel('Saving remote'));
                    $scope.saveGadget();
                }, function() {
                    $state.go(rdb.is.state + 'gadget', {
                        widgetId: $stateParams.widgetId,
                        gadgetId: $stateParams.gadgetId
                    });
                });
            } else {
                $state.go(rdb.is.state + 'gadget', {
                    widgetId: $stateParams.widgetId,
                    gadgetId: $stateParams.gadgetId
                });
            }
        };

        function seperator(list, next) {
            if (list.length > 0) {
                var last = list.pop();
                list.push(last);
                if (last !== next && !(last == 'pagebreak' && next == 'rowbreak'))
                    list.push(next);
            }
        }

        $scope.saveGadget = function() {
            showPrompt('Saving...');
            var layout = [];
            $scope.buttongroup.forEach(function(bg) {
                bg.forEach(function(rg) {
                    rg.forEach(function(btn) {
                        layout.push(btn.key);
                    });
                    seperator(layout, 'rowbreak');
                });
                seperator(layout, 'pagebreak');
            });
            // console.log(layout);
            $scope.remote.layout = layout;
            delete($scope.remote.dirty);
            rdb.saveRemote($scope.widget, $scope.gadget, $scope.remote)
                .then(function() {
                    hidePrompt();
                    delete($scope.gadget.editremote);
                    $state.go(rdb.is.state + 'gadget', {
                        widgetId: $stateParams.widgetId,
                        gadgetId: $stateParams.gadgetId
                    });
                });
        }

        $scope.addButtons = function(ev) {
            $scope.mode.edit = false;
            $scope.comingFromEditMode = true;
            $state.go(rdb.is.state + 'editgadget.addbuttons', {
                widgetId: $stateParams.widgetId,
                gadgetId: $stateParams.gadgetId
            });
            return;
        };

        removeMarks = function() {
            $scope.buttongroup.forEach(function(page) {
                page.mark = false;
                page.forEach(function(brow) {
                    brow.mark = false;
                    brow.forEach(function(belem) {
                        belem.mark = false;
                    })
                })
            })
        };

        $scope.markElement = function(event, elem, pageIndex, rowIndex, buttonIndex) {
            //if (event.type !== "click") return;
            //buttonelem.mark = !buttonelem.mark;
            var val = elem.mark;
            removeMarks();
            elem.mark = !val;
            $scope.markedButton = $scope.markedRow = $scope.markedPage = null;

            if (buttonIndex >= 0) { // button clicked
                $scope.markedButton = (elem.mark) ? elem : null;
            } else if (rowIndex >= 0) { // row clicked
                $scope.markedRow = (elem.mark) ? elem : null;
            } else if (pageIndex >= 0) { // page clicked
                $scope.markedPage = (elem.mark) ? elem : null;
            }
        };

        $scope.editButton = function(dragButton) {
            var bindex = _.findIndex($scope.remote.keys, function(key) {
                return key.key === dragButton.key;
            })
            if (bindex >= 0) {
                $state.go(rdb.is.state + 'editbutton', {
                    widgetId: $stateParams.widgetId,
                    gadgetId: $stateParams.gadgetId,
                    buttonIndex: bindex
                });
            }
        };

        $scope.removeEmptyItems = function(bgroup) {
            for (pnum = bgroup.length - 1; pnum >= 0; pnum--) {
                var bpage = bgroup[pnum];
                for (rnum = bpage.length - 1; rnum >= 0; rnum--) {
                    if (bpage[rnum].length <= 0) {
                        // empty row. delete
                        bpage.splice(rnum, 1);
                    }
                }
                if (bpage.length <= 0) {
                    // empty page. delete
                    bgroup.splice(pnum, 1);
                }
            }
        };

        $scope.addBlankButton = function() {
            //add blank button

            //  find the last blank button added
            var newkey = 1;
            var keys = $scope.remote.keys;
            for (i = 0; i < keys.length; i++) {
                if (keys[i].key.substr(0, 'newkey_'.length) === 'newkey_')
                    newkey = parseInt(keys[i].key.substr('newkey_'.length)) + 1;
            }

            var belem = {};
            belem.key = "newkey_" + newkey;
            $scope.remote.keys.push(belem);
            $scope.buttongroup[0].splice(0, 0, [belem]);
            $scope.remote.layout.splice(0, 0, belem.key);
            $scope.markElement(null, belem, 0, 0, 0);
            $scope.remote.dirty = true;
        };

        $scope.getMarkedElement = function() {
            var markedElement = {
                pageIndex: -1,
                rowIndex: -1,
                buttonIndex: -1
            };
            var found = false;
            for (p = 0; p < $scope.buttongroup.length && !found; p++) {
                markedElement.pageIndex = p;
                markedElement.rowIndex = -1;
                markedElement.buttonIndex = -1;
                if ($scope.buttongroup[p].mark) {
                    found = true;
                } else {
                    var page = $scope.buttongroup[p];
                    for (r = 0; r < page.length && !found; r++) {
                        markedElement.rowIndex = r;
                        markedElement.buttonIndex = -1;

                        if (page[r].mark) {
                            found = true;
                        } else {
                            var row = page[r];
                            for (b = 0; b < row.length && !found; b++) {
                                markedElement.buttonIndex = b;
                                if (row[b].mark) {
                                    found = true;
                                }
                            }
                        }
                    }
                }
            }
            return (found) ? markedElement : null;
        };

        $scope.delete = function() {
            var markedElement = $scope.getMarkedElement();
            if (!markedElement) return;
            var pindex = markedElement.pageIndex;
            var rindex = markedElement.rowIndex;
            var bindex = markedElement.buttonIndex;
            removeMarks();
            if (bindex >= 0) { // button removal
                $scope.buttongroup[pindex][rindex].splice(bindex, 1);
            } else if (rindex >= 0) {
                $scope.buttongroup[pindex].splice(rindex, 1);
            } else if (pindex >= 0) {
                $scope.buttongroup.splice(pindex, 1);
            }
            $scope.remote.dirty = true;
            $scope.removeEmptyItems($scope.buttongroup);
        }

        $scope.addToBlankPage = function() {
            var markedElement = $scope.getMarkedElement();
            if (!markedElement) return;
            var pindex = markedElement.pageIndex;
            var rindex = markedElement.rowIndex;
            var bindex = markedElement.buttonIndex;
            removeMarks();
            if (bindex >= 0) { // button
                var belem = $scope.buttongroup[pindex][rindex][bindex];
                $scope.buttongroup[pindex][rindex].splice(bindex, 1);
                $scope.buttongroup.splice(0, 0, [
                    [belem]
                ]);
                $scope.markElement(0, belem, 0, 0, 0);
            } else if (rindex >= 0) {
                var row = $scope.buttongroup[pindex][rindex];
                $scope.buttongroup[pindex].splice(rindex, 1);
                $scope.buttongroup.splice(0, 0, row);
                $scope.markElement(0, row, 0, 0);
            }
            $scope.remote.dirty = true;
            $scope.removeEmptyItems($scope.buttongroup);
        };

        $scope.addToBlankRow = function() {
            var markedElement = $scope.getMarkedElement();
            if (!markedElement) return;
            var pindex = markedElement.pageIndex;
            var rindex = markedElement.rowIndex;
            var bindex = markedElement.buttonIndex;
            removeMarks();
            if (bindex >= 0) { // button
                var belem = $scope.buttongroup[pindex][rindex][bindex];
                $scope.buttongroup[pindex][rindex].splice(bindex, 1);
                $scope.buttongroup[pindex].splice(0, 0, [belem]);
                $scope.markElement(0, belem, pindex, 0, 0);
            }
            $scope.remote.dirty = true;
            $scope.removeEmptyItems($scope.buttongroup);
        };

        $scope.move = function(command) {
            var markedElement = $scope.getMarkedElement();
            if (!markedElement) return;
            var pindex = markedElement.pageIndex;
            var rindex = markedElement.rowIndex;
            var bindex = markedElement.buttonIndex;
            var newpindex = pindex;
            var newrindex = rindex;
            var newbindex = bindex;
            var dirty = false;
            var elem = null;

            if (bindex >= 0) { // button move
                switch (command) {
                    case 'left':
                        newbindex--;
                        break;
                    case 'up':
                        newrindex--;
                        break;
                    case 'right':
                        newbindex++;
                        break;
                    case 'down':
                        newrindex++;
                        break;
                }
                if (newbindex < 0) return;
                if (newbindex > ($scope.buttongroup[pindex][rindex].length - 1)) return;

                if (newrindex < 0) newpindex--;
                if (newpindex < 0) return;

                if (newrindex > ($scope.buttongroup[pindex].length - 1)) newpindex++;
                if (newpindex > ($scope.buttongroup.length - 1)) return;

                if (newpindex != pindex) { // page changed
                    if (newpindex > pindex) { // button moving to next page, first row, first button
                        newrindex = 0;
                        newbindex = 0;
                    }
                    if (newpindex < pindex) { //button moving to prev page, last row, last button
                        newrindex = $scope.buttongroup[newpindex].length-1; // last row
                        newbindex = $scope.buttongroup[newpindex][newrindex].length;
                    }
                } else if (newrindex != rindex) { // row changed
                    if (newrindex > rindex) { // button moving to next row
                        newbindex = 0;
                    }
                    if (newrindex < rindex) { // button moving to prev row
                        newbindex = $scope.buttongroup[newpindex][newrindex].length;
                    }
                }

                if (pindex != newpindex || rindex != newrindex || bindex != newbindex) {
                    // save,remove, insert
                    var belem = $scope.buttongroup[pindex][rindex][bindex];
                    $scope.buttongroup[pindex][rindex].splice(bindex, 1);
                    $scope.buttongroup[newpindex][newrindex].splice(newbindex, 0, belem);
                    dirty = true;
                    elem = belem;
                }
            } else if (rindex >= 0) { //row move
                switch (command) {
                    //case 'left': newbindex--; break;
                    case 'up':
                        newrindex--;
                        break;
                        //case 'right': newbindex++;break;
                    case 'down':
                        newrindex++;
                        break;
                }

                if (newrindex < 0) newpindex--;
                if (newpindex < 0) return;

                if (newrindex > ($scope.buttongroup[pindex].length - 1)) newpindex++;
                if (newpindex > ($scope.buttongroup.length - 1)) return;

                if (newpindex > pindex) { // button moving to next page, first row, first button
                    newrindex = 0;
                }
                if (newpindex < pindex) { //button moving to prev page, last row, last button
                    newrindex = $scope.buttongroup[newpindex].length;
                }
                if (pindex != newpindex || rindex != newrindex) { // save,remove, insert
                    var row = $scope.buttongroup[pindex][rindex];
                    $scope.buttongroup[pindex].splice(rindex, 1);
                    $scope.buttongroup[newpindex].splice(newrindex, 0, row);
                    dirty = true;
                    elem = row;
                }
            } else if (pindex >= 0) { //page move
                switch (command) {
                    //case 'left':
                    case 'up':
                        if (pindex > 0) {
                            newpindex = pindex - 1;
                        }
                        break;
                    //case 'right':
                    case 'down':
                        if (newpindex < ($scope.buttongroup.length - 1)) {
                            newpindex = pindex + 1;
                        }
                        break;
                }
                if (pindex != newpindex) {
                    // save,remove, insert
                    var page = $scope.buttongroup[pindex];
                    $scope.buttongroup.splice(pindex, 1);
                    $scope.buttongroup.splice(newpindex, 0, page);
                    dirty = true;
                    elem = page;
                }
            }
            if (dirty) {
                $scope.remote.dirty = true;
                $scope.removeEmptyItems($scope.buttongroup);
            }
        };

        var toastVisible = false;
        var showPrompt = function(message) {
            if (toastVisible)
                $mdToast.updateContent(message);
            else {
                toastVisible = true;
                $mdToast.show($mdToast.simple()
                    .content(message)
                    .hideDelay(0)
                    .position('top right'));
            }
        };
        var hidePrompt = function() {
            $mdToast.hide();
            toastVisible = false;
        };

        function delay(t) {
            var d = $q.defer();
            $timeout(function() {
                d.resolve(true);
            }, t);
            return d.promise;
        }

        $scope.startLearning = function() {
            if (!$scope.learning) {
                // Prepare a list of keys to learn
                $scope.keysToLearn = [];
                // if a button is selected start from there
                var start = false;
                if (!$scope.markedButton) start = true;
                $scope.buttongroup.forEach(function(bg) {
                    bg.forEach(function(rg) {
                        rg.forEach(function(btn) {
                            if ($scope.markedButton && $scope.markedButton.key === btn.key) {
                                start = true;
                            }
                            if (start) $scope.keysToLearn.push(btn);
                        });
                    });
                });
                if ($scope.keysToLearn.length == 0) {
                    $mdToast.show($mdToast.simple('Please add some keys to your remote first').position('top right'));
                    return;
                }

                $scope.learning = true;
                $scope.ktlIndex = 0;
                $scope.ktlCount = 0;

                var readIrCode = function() {
                    // FIXME: Scroll
                    $scope.keysToLearn[$scope.ktlIndex].learning = true;
                    showPrompt('Press highlighted key on your real remote');
                    wid.getKeyPress($scope.widget)
                        .then(function(data) {
                            if ($scope.ktlIndex !== undefined) {
                                if (data) {
                                    $scope.keysToLearn[$scope.ktlIndex].learning = false;
                                    $scope.keysToLearn[$scope.ktlIndex].analyzing = true;
                                    showPrompt('Analyzing...');
                                    return rdb.getKeyCodes(data);
                                } else {
                                    // Timed out / cancelled
                                    $scope.stopLearning();
                                }
                            }
                            // Else, already cancelled
                        })
                        .then(function(codes) {
                            if ($scope.ktlIndex !== undefined) {
                                $scope.keysToLearn[$scope.ktlIndex].analyzing = false;
                                if (codes) {
                                    // Check confidence and re-learn if required
                                    if (codes.confidence == 0 && $scope.ktlCount < 2) {
                                        showPrompt('Could not learn the key correctly. Let\'s try again.');
                                        // Re-learn
                                        $scope.ktlCount++;
                                        $timeout(readIrCode, 2000);
                                    } else {
                                        // Save codes for current key
                                        var key = $scope.keysToLearn[$scope.ktlIndex];
                                        delete(key.spec);
                                        delete(key.tcode);
                                        _.extend(key, codes);
                                        showPrompt('Key learnt successfully.');
                                        $scope.remote.dirty = true;
                                        $scope.ktlIndex++;
                                        if ($scope.learning && $scope.ktlIndex < $scope.keysToLearn.length) {
                                            // Schedule next key
                                            $scope.ktlCount = 0;
                                            $timeout(readIrCode, 1000);
                                        } else {
                                            $timeout($scope.stopLearning, 1000);
                                        }
                                    }
                                }
                                // Else, cancelled
                            }
                            // Else, cancelled
                        })
                        .catch(function(error) {
                            console.log('*** readIrCode:', error);
                            $scope.stopLearning();
                        });
                };

                readIrCode();
            }
        };

        $scope.stopLearning = function() {
            if ($scope.widget) wid.cancelKeyPress($scope.widget);
            if ($scope.ktlIndex !== undefined && $scope.keysToLearn[$scope.ktlIndex]) {
                $scope.keysToLearn[$scope.ktlIndex].learning = false;
                $scope.keysToLearn[$scope.ktlIndex].analyzing = false;
            }
            delete($scope.ktlIndex);
            hidePrompt();
            $scope.learning = false;
            return;
        }

        $scope.skipPrevKey = function() {
            if ($scope.ktlIndex !== undefined) {
                if ($scope.keysToLearn[$scope.ktlIndex]) {
                    $scope.keysToLearn[$scope.ktlIndex].learning = false;
                    $scope.keysToLearn[$scope.ktlIndex].analyzing = false;
                }
                if ($scope.ktlIndex > 0) $scope.ktlIndex--;
                if ($scope.keysToLearn[$scope.ktlIndex]) {
                    $scope.keysToLearn[$scope.ktlIndex].learning = true;
                    $scope.keysToLearn[$scope.ktlIndex].analyzing = false;
                } else {
                    $scope.stopLearning();
                }
            }
        }

        $scope.skipNextKey = function() {
            if ($scope.ktlIndex !== undefined) {
                if ($scope.keysToLearn[$scope.ktlIndex]) {
                    $scope.keysToLearn[$scope.ktlIndex].learning = false;
                    $scope.keysToLearn[$scope.ktlIndex].analyzing = false;
                }
                $scope.ktlIndex++;
                if ($scope.keysToLearn[$scope.ktlIndex]) {
                    $scope.keysToLearn[$scope.ktlIndex].learning = true;
                    $scope.keysToLearn[$scope.ktlIndex].analyzing = false;
                } else {
                    $scope.stopLearning();
                }
            }
        }

        // mv items
        $scope.mvitems = [{
            icon: "keyboard_arrow_left",
            purpose: "move left",
            show: false,
            func: function() {
                $scope.move('left');
            },
            tooltip: "move left",
        }, {
            icon: "keyboard_arrow_down",
            purpose: "movedown",
            show: false,
            func: function() {
                $scope.move('down');
            },
            tooltip: "move down",
        }, {
            icon: "keyboard_arrow_up",
            purpose: "moveup",
            show: false,
            func: function() {
                $scope.move('up');
            },
            tooltip: "move up",
        }, {
            icon: "keyboard_arrow_right",
            purpose: "moveright",
            show: false,
            func: function() {
                $scope.move('right');
            },
            tooltip: "move right",
        }];

        // footer items
        $scope.fitems = [{
            icon: "my_library_books",
            name: "add page",
            purpose: "addpage",
            show: false,
            func: function() {
                $scope.addToBlankPage();
            },
            tooltip: "add to an empty page",
        }, {
            icon: "my_library_add",
            name: "add row",
            purpose: "addrow",
            show: false,
            func: function() {
                $scope.addToBlankRow();
            },
            tooltip: "add to an empty row",
        }, {
            icon: "edit",
            name: "edit",
            purpose: "edit",
            show: false,
            func: function() {
                $scope.editButton($scope.markedButton);
            },
            tooltip: "edit button",
        }, {
            icon: "delete",
            name: "delete",
            purpose: "delete",
            show: false,
            tooltip: "delete",
            func: function() {
                $scope.delete()
            },
        }, {
            icon: "playlist_add",
            name: "add",
            purpose: "add",
            show: true,
            func: $scope.addButtons,
            tooltip: "add buttons",
        }, {
            icon: "add",
            name: "blank",
            purpose: "blankbutton",
            show: false,
            func: $scope.addBlankButton,
            tooltip: "add blank button",
        }, {
            icon: "hearing",
            name: "learn",
            purpose: "learn",
            show: true,
            func: $scope.startLearning,
            tooltip: "Learn codes from a real remote",
        }, {
            icon: "skip_previous",
            name: "previous",
            purpose: "previous",
            show: false,
            func: $scope.skipPrevKey,
            tooltip: "skip previous",
        }, {
            icon: "skip_next",
            name: "next",
            purpose: "next",
            show: false,
            func: $scope.skipNextKey,
            tooltip: "skip next",
        }, {
            icon: "stop",
            name: "stop",
            purpose: "stop",
            show: false,
            func: $scope.stopLearning,
            tooltip: "stop learning",
        }];

        var handleFooterButtons = function() {
            $scope.mode.move = ($scope.markedButton || $scope.markedRow || $scope.markedPage) ? true : false;

            $scope.mvitems.forEach(function(mvitem) {
                mvitem.show = $scope.mode.move;
            });

            $scope.fitems.forEach(function(fitem) {
                fitem.show = false;
                if (!$scope.learning) {
                    if (fitem.purpose === "add" && !($scope.markedButton || $scope.markedRow || $scope.markedPage))
                        fitem.show = true;
                    if (fitem.purpose === "learn")
                        fitem.show = true;
                    if (fitem.purpose === "blankbutton" && !($scope.markedButton || $scope.markedRow || $scope.markedPage))
                        fitem.show = true;
                    if (fitem.purpose === "edit" && $scope.markedButton)
                        fitem.show = true;
                    if (fitem.purpose === "addpage" && ($scope.markedButton || $scope.markedRow))
                        fitem.show = true;
                    if (fitem.purpose === "addrow" && $scope.markedButton)
                        fitem.show = true;
                    if (($scope.markedButton || $scope.markedRow || $scope.markedPage) && fitem.purpose == 'delete') {
                        fitem.show = true;
                    }
                } else {
                    if (fitem.purpose === "previous" || fitem.purpose === "next" || fitem.purpose === "stop")
                        fitem.show = true;
                }
            })
        };

        $scope.$watch(function() {
            return $scope.learning;
        }, function() {
            handleFooterButtons();
        });
        $scope.$watch(function() {
            return $scope.markedButton || $scope.markedRow || $scope.markedPage;
        }, function() {
            handleFooterButtons();
        });

        $scope.menuitems = [{
            name: "done",
            func: $scope.goBack
        }];

        $scope.menuClick = function(menuitem) {
            if (menuitem.func) menuitem.func(menuitem);
        };
        $scope.$on("$destroy", function() {
            $mdDialog.cancel();
        });
        $mdToast.show($mdToast.simple("Click and Move buttons/rows to re-arrage").position('top right'));

        $scope.valueChanged = function() {
            $scope.remote.dirty = true;
        };

        autorouter();
    })
    .controller("EditFooterController", function($scope) {
        $scope.isFooterButton = true;
    });
